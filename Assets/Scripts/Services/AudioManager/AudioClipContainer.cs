using UnityEngine;

namespace HyperRacer.Services.AudioManager
{
    [CreateAssetMenu(menuName = "Hyper Racer/Audio Manager/Create Audio Clip Container")]
    public class AudioClipContainer : ScriptableObject
    {
        [SerializeField]
        private string audioClipIdentifier = "";

        [SerializeField]
        private AudioClip audioClip = null;

        [SerializeField]
        private bool loop = false;

        [SerializeField]
        [Range(0.0f, 1.0f)]
        private float volume = 1.0f;

        [SerializeField]
        [Range(-3.0f, 3.0f)]
        private float pitch = 1.0f;

        [SerializeField]
        [Range(-1.0f, 1.0f)]
        private float stereoPan = 0.0f;

        [SerializeField]
        [Range(0.0f, 1.0f)]
        private float spatialBlend = 0.0f;

        [SerializeField]
        [Range(0.0f, 1.1f)]
        private float reverbZoneMix = 1.0f;

        public string AudioClipIdentifier
        {
            get => audioClipIdentifier;
            private set => audioClipIdentifier = value;
        }

        public AudioClip AudioClip
        {
            get => audioClip;
            private set => audioClip = value;
        }

        public bool Loop
        {
            get => loop;
            private set => loop = value;
        }

        public float Volume
        {
            get => volume;
            private set => volume = value;
        }

        public float Pitch
        {
            get => pitch;
            private set => pitch = value;
        }

        public float StereoPan
        {
            get => stereoPan;
            private set => stereoPan = value;
        }

        public float SpatialBlend
        {
            get => spatialBlend;
            private set => spatialBlend = value;
        }

        public float ReverbZoneMix
        {
            get => reverbZoneMix;
            private set => reverbZoneMix = value;
        }
    }
}