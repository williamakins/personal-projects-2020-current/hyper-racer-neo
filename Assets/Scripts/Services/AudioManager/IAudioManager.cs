using HyperRacer.Core.ServiceLocator;
using UnityEngine;

namespace HyperRacer.Services.AudioManager
{
    public interface IAudioManager : IService
    {
        public AudioSource PlaySound(string sound, ulong delay = default);
        public void StopSound(string sound);
        public void StopAllSounds();
        public void PauseSound(string sound);
        public void UnPauseSound(string sound);
    }
}