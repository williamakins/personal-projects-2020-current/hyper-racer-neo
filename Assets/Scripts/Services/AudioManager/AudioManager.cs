using HyperRacer.Core.ServiceLocator;
using UnityEngine;
using UnityEngine.Assertions;

namespace HyperRacer.Services.AudioManager
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(AudioListener))]
    public class AudioManager : MonoBehaviour, IAudioManager
    {
        [SerializeField]
        private int audioSourcePoolSize = 30;

        [SerializeField]
        private AudioSource defaultAudioSource = null;

        [SerializeField]
        private AudioClipContainer[] audioClipContainers = null;

        private AudioSource[] audioSources = null;

        private void Awake()
        {
            ServiceLocator.Current.Register<IAudioManager>(this);

            InitializeAudioSources();
        }

        private void Destroy()
        {
            ServiceLocator.Current.Deregister<IAudioManager>();
        }

        public AudioSource PlaySound(string sound, ulong delay = default)
        {
            if (string.IsNullOrWhiteSpace(sound))
            {
                Debug.LogWarning("AudioManager :: Can't play sound as the sound identifier is null or empty");
                return null;
            }

            AudioSource audioSource = FindFreeAudioSource();
            AudioClipContainer audioClipContainer = FindAudioClipContainer(sound);

            if (audioSource != null && audioClipContainer != null)
            {
                ConfigureAudioSource(audioSource, audioClipContainer);
                PlaySoundInternal(audioSource, delay);

                return audioSource;
            }

            return null;
        }

        public void StopSound(string sound)
        {
            if (string.IsNullOrWhiteSpace(sound))
            {
                Debug.LogWarning("AudioManager :: Can't stop sound as the sound identifier is null or empty");
                return;
            }

            AudioSource source = FindAudioSource(sound);

            if (source != null)
            {
                source.Stop();
                source.gameObject.SetActive(false);
            }
        }

        public void StopAllSounds()
        {
            if (audioSources?.Length > 0)
            {
                for (int i = 0; i < audioSources.Length; i++)
                {
                    if (audioSources[i].isPlaying)
                    {
                        audioSources[i].Stop();
                        audioSources[i].gameObject.SetActive(false);
                    }
                }
            }
            else
            {
                Debug.LogWarning("AudioManager :: Can't stop audio sources as audio source array is null or empty");
            }
        }

        public void PauseSound(string sound)
        {
            if (string.IsNullOrWhiteSpace(sound))
            {
                Debug.LogWarning("AudioManager :: Can't pause sound as the sound identifier is null or empty");
                return;
            }

            AudioSource source = FindAudioSource(sound);

            if (source != null)
            {
                source.Pause();
            }
        }

        public void UnPauseSound(string sound)
        {
            if (string.IsNullOrWhiteSpace(sound))
            {
                Debug.LogWarning("AudioManager :: Can't resume sound as the sound identifier is null or empty");
                return;
            }

            AudioSource source = FindAudioSource(sound);

            if (source != null)
            {
                source.UnPause();
            }
        }

        private void InitializeAudioSources()
        {
            Assert.IsNotNull(defaultAudioSource);

            audioSources = new AudioSource[audioSourcePoolSize];
            for (int i = 0; i < audioSourcePoolSize; i++)
            {
                audioSources[i] = Instantiate<AudioSource>(defaultAudioSource, Vector3.zero, Quaternion.identity, gameObject.transform);
                audioSources[i].gameObject.SetActive(false);
            }
        }

        private AudioSource FindFreeAudioSource()
        {
            if (audioSources?.Length > 0)
            {
                for (int i = 0; i < audioSources.Length; i++)
                {
                    if (!audioSources[i].isPlaying)
                    {
                        return audioSources[i];
                    }
                }

                Debug.LogWarning("AudioManager :: Unable to find any free audio sources");
                return null;
            }

            Debug.LogError("AudioManager :: Can't find free audio source as audio source array is null or empty");
            return null;
        }

        private AudioSource FindAudioSource(string soundIdentifier)
        {
            if (audioSources?.Length > 0)
            {
                for (int i = 0; i < audioSources.Length; i++)
                {
                    if (audioSources[i].isPlaying && audioSources[i].name == soundIdentifier)
                    {
                        return audioSources[i];
                    }
                }

                return null;
            }

            Debug.LogError("AudioManager :: Can't find audio source as audio source array is null or empty");
            return null;
        }

        private AudioClipContainer FindAudioClipContainer(string soundIdentifier)
        {
            if (audioClipContainers?.Length > 0)
            {
                for (int i = 0; i < audioClipContainers.Length; i++)
                {
                    if (string.Equals(soundIdentifier, audioClipContainers[i].AudioClipIdentifier))
                    {
                        return audioClipContainers[i];
                    }
                }

                Debug.LogWarning($"AudioManager :: Unable to find any audio clips with a matching identifier of {soundIdentifier}");
                return null;
            }

            Debug.LogError("AudioManager :: Can't find any audio clip containers as array is null or empty");
            return null;
        }

        private void ConfigureAudioSource(AudioSource audioSource, AudioClipContainer audioClipContainer)
        {
            audioSource.name = audioClipContainer.AudioClipIdentifier;
            audioSource.clip = audioClipContainer.AudioClip;
            audioSource.loop = audioClipContainer.Loop;
            audioSource.volume = audioClipContainer.Volume;
            audioSource.pitch = audioClipContainer.Pitch;
            audioSource.panStereo = audioClipContainer.StereoPan;
            audioSource.spatialBlend = audioClipContainer.SpatialBlend;
            audioSource.reverbZoneMix = audioClipContainer.ReverbZoneMix;
        }

        private void PlaySoundInternal(AudioSource audioSource, ulong delay)
        {
            if (audioSource == null)
            {
                return;
            }

            audioSource.gameObject.SetActive(true);
            audioSource.Play(delay);
        }
    }
}