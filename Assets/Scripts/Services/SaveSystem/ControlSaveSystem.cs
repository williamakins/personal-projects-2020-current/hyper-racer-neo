﻿using HyperRacer.Save;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlSaveSystem : ISaveSystem
{

    private float cameraX = 0.0f;
    private float cameraY = 0.0f;

    public float CameraY { get => cameraY; set => cameraY = value; }
    public float CameraX { get => cameraX; set => cameraX = value; }

    public void Load()
    {
        cameraX = SaveUtils.GetFloat("cameraX", 50.0f);
        cameraY = SaveUtils.GetFloat("cameraY", 50.0f);
    }

    public void Reset()
    {
        cameraX = 50.0f;
        cameraY = 50.0f;
    }

    public void Save()
    {
        SaveUtils.SetFloat("cameraX", cameraX);
        SaveUtils.SetFloat("cameraY", cameraY);
    }
}
