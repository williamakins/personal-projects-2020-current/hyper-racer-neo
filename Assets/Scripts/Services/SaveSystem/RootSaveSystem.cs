﻿using HyperRacer.Core.ServiceLocator;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootSaveSystem : MonoBehaviour, ISaveSystem, IService
{
    private List<ISaveSystem> saves = new List<ISaveSystem>();
    private ControlSaveSystem controlSaveSystem = new ControlSaveSystem();
    private SettingsSaveSystem settingsSaveSystem = new SettingsSaveSystem();

    public ControlSaveSystem ControlSaveSystem { get => controlSaveSystem; }
    public SettingsSaveSystem SettingsSaveSystem { get => settingsSaveSystem; }

    private void Awake()
    {
        ServiceLocator.Current.Register(this);
        saves.Add(controlSaveSystem);
        saves.Add(settingsSaveSystem);

        Load();
    }

    private void OnDestroy()
    {
        ServiceLocator.Current.Deregister<RootSaveSystem>();
    }

    public void Load()
    {
        for(int i = 0; i < saves.Count; i++)
        {
            saves[i].Load();
        }
    }

    public void Reset()
    {
        for (int i = 0; i < saves.Count; i++)
        {
            saves[i].Reset();
        }
    }

    public void Save()
    {
        for (int i = 0; i < saves.Count; i++)
        {
            saves[i].Save();
        }
    }
}
