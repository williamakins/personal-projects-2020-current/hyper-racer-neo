using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HyperRacer.Save
{
    public static class SaveUtils
    {
        public static int GetInt(string name, int defaultValue)
        {
            int toReturn = defaultValue;
            if (HasKey(name))
            {
                toReturn = PlayerPrefs.GetInt(name, defaultValue);
            }
            return toReturn;
        }

        public static float GetFloat(string name, float defaultValue)
        {
            float toReturn = defaultValue;
            if (HasKey(name))
            {
                toReturn = PlayerPrefs.GetFloat(name, defaultValue);
            }
            return toReturn;
        }

        public static string GetString(string name, string defaultValue)
        {
            string toReturn = defaultValue;
            if (HasKey(name))
            {
                toReturn = PlayerPrefs.GetString(name, defaultValue);
            }
            return toReturn;
        }

        public static void SetInt(string name, int value)
        {
            PlayerPrefs.SetInt(name, value);
        }

        public static void SetFloat(string name, float value)
        {
            PlayerPrefs.SetFloat(name, value);
        }

        public static void SetString(string name, string value)
        {
            PlayerPrefs.SetString(name, value);
        }

        public static void Save()
        {
            PlayerPrefs.Save();
        }

        public static bool HasKey(string key)
        {
            return PlayerPrefs.HasKey(key);
        }
    }
}

