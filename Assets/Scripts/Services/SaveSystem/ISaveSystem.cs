﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISaveSystem
{
    void Load();
    void Save();
    void Reset();
}
