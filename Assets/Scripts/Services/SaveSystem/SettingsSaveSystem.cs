﻿using HyperRacer.Save;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsSaveSystem : ISaveSystem
{

    private float musicVolume = 1.0f;
    private float masterVolume = 1.0f;
    private float sfxVolume = 1.0f;

    private int qualityLevel = 1;

    public float MusicVolume { get => musicVolume; set => musicVolume = value; }
    public float MasterVolume { get => masterVolume; set => masterVolume = value; }
    public float SfxVolume { get => sfxVolume; set => sfxVolume = value; }
    public int QualityLevel { get => qualityLevel; set => qualityLevel = value; }

    public void Load()
    {

        //Audio
        musicVolume = SaveUtils.GetFloat("musicVolume", 1.0f);
        masterVolume = SaveUtils.GetFloat("masterVolume", 1.0f);
        sfxVolume = SaveUtils.GetFloat("sfxVolume", 1.0f);

        //Game Quality
        qualityLevel = SaveUtils.GetInt("qualityLevel", QualitySettings.GetQualityLevel());
    }

    public void Reset()
    {
        //Audio
        musicVolume = 1.0f;
        masterVolume = 1.0f;
        sfxVolume = 1.0f;

        //Game Quality
        qualityLevel = 1;
    }

    public void Save()
    {
        //Audio
        SaveUtils.SetFloat("musicVolume", musicVolume);
        SaveUtils.SetFloat("masterVolume", masterVolume);
        SaveUtils.SetFloat("sfxVolume", sfxVolume);

        //Game Quality
        SaveUtils.SetInt("qualityLevel", qualityLevel);
    }
}
