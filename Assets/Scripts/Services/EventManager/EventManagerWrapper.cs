using HyperRacer.Core.ServiceLocator;
using LudumDare.Core.EventManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManagerWrapper : EventManager<Events>, IEventManager<Events>
{
    private void Awake()
    {
        ServiceLocator.Current.Register<IEventManager<Events>>(this);
    }
}
