using HyperRacer.Core.ServiceLocator;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEventManager<T> : IService
{
    public void RegisterEvent(T eventName, Action<T, object[]> action);
    public void DeregisterEvent(T eventName, Action<T, object[]> action);
    public void TriggerEvent(T eventName, params object[] objects);
}
