﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LudumDare.Core.EventManager
{
    public class EventManager<T> : MonoBehaviour, IEventManager<T>
    {
        protected Dictionary<T, List<Action<T, object[]>>> TDictionary = new Dictionary<T, List<Action<T, object[]>>>();
        public void RegisterEvent(T eventName, Action<T, object[]> action)
        {
            if (TDictionary.ContainsKey(eventName))
            {
                TDictionary[eventName].Add(action);
            }
            else
            {
                List<Action<T, object[]>> list = new List<Action<T, object[]>>();
                list.Add(action);
                TDictionary.Add(eventName, list);
            }
        }

        public void DeregisterEvent(T eventName, Action<T, object[]> action)
        {
            if (TDictionary.ContainsKey(eventName))
            {
                if (TDictionary[eventName].Contains(action))
                {
                    TDictionary[eventName].Remove(action);
                }
                else
                {
                    Debug.Log("EventManager - " + action.ToString() + " not found in list.");
                }
            }
            else
            {
                Debug.Log("EventManager - " + eventName.ToString() + " not found in dictionary.");
            }
        }

        public void TriggerEvent(T eventName, params object[] objects)
        {
            if (TDictionary.ContainsKey(eventName))
            {
                foreach (Action<T, object[]> action in TDictionary[eventName])
                {
                    action(eventName, objects);
                }
            }
            else
            {
                Debug.Log("EventManager - " + eventName.ToString() + " not found in dictionary.");
            }
        }
    }
}