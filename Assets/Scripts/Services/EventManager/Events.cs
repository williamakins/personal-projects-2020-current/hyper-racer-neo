﻿namespace LudumDare.Core.EventManager
{
    public enum Events
    {
        OnLap,
        FailConditionMet,
        FailConditionFinished,
        OnMapExit,
        StartCountdown,
        ResetMap
    }
}