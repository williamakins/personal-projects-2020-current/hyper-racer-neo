﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace HyperRacer.Core.Initialization.Editor
{
    [InitializeOnLoad]
    public static class BootGameDebug
    {
        private const int bootSceneIndex = 0;
        private const string sceneBeforePlayKey = "SceneBeforePlay";

        static BootGameDebug()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChange;
        }

        private static void OnPlayModeStateChange(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.ExitingEditMode)
            {
                EditorPrefs.SetString(sceneBeforePlayKey, EditorSceneManager.GetActiveScene().path);

                EditorSceneManager.OpenScene(SceneUtility.GetScenePathByBuildIndex(bootSceneIndex), OpenSceneMode.Single);
                EditorApplication.playModeStateChanged -= OnPlayModeStateChange;
            }
            else if (state == PlayModeStateChange.EnteredEditMode)
            {
                EditorSceneManager.OpenScene(EditorPrefs.GetString(sceneBeforePlayKey), OpenSceneMode.Single);
            }
        }
    }
}