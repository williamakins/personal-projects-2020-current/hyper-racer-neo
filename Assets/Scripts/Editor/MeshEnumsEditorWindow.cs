﻿using SplineMesh;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEngine;

namespace HyperRacer.Editor.MeshEnums
{
    public class MeshEnumsEditorWindow : EditorWindow
    {
        private MeshEnumsStaticData.MeshEnum selectedEnum = MeshEnumsStaticData.MeshEnum.Standard;

        private Color[] meshColors = null;
        private int[] meshTriangles = null;

        private GameObject rootObject = null;
        private SplineMeshData splineMeshData = null;
        private MeshFilter meshFilter = null;
        private Mesh mesh = null;

        private bool dataLoaded = false;
        private bool mouseHeld = false;

        [MenuItem("Hyper Racer/Vertex Enum Editor")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(MeshEnumsEditorWindow), true, "Mesh Enums Editor", true);
        }

        private void OnEnable()
        {
            SceneView.duringSceneGui += OnSceneGUI;

            PrefabStage prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
            if (prefabStage != null)
            {
                rootObject = prefabStage.prefabContentsRoot;
            }

            if (rootObject != null)
            {
                if (rootObject.transform.Find("EnumsMesh").TryGetComponent(out MeshFilter meshFilter))
                {
                    this.meshFilter = meshFilter;
                }
            }

            if (meshFilter != null && dataLoaded == false)
            {
                GetSplineMeshData();

                GetMeshData();
            }
        }

        private void OnDisable()
        {
            if (splineMeshData != null)
            {
                EditorUtility.SetDirty(splineMeshData);
            }

            SceneView.duringSceneGui -= OnSceneGUI;
        }

        private void GetSplineMeshData()
        {
            if (rootObject.TryGetComponent(out SplineMeshData splineMeshData))
            {
                this.splineMeshData = splineMeshData;
            }
            else
            {
                Debug.LogWarning("ERROR: Root object does not have SplineMeshData component");
            }
        }

        private void GetMeshData()
        {
            mesh = meshFilter.sharedMesh;

            if (mesh != null && splineMeshData != null)
            {
                meshColors = new Color[mesh.vertexCount];
                meshTriangles = mesh.triangles;

                //if the enum list is empty, then populate it
                if (splineMeshData.vertexEnums == null || splineMeshData.vertexEnums.Length == 0)
                {
                    splineMeshData.vertexEnums = new int[mesh.vertexCount];
                }

                MeshEnumsStaticData.PopulateDict();
                UpdateAllColors();

                dataLoaded = true;
            }
        }

        private void OnGUI()
        {
            if (!dataLoaded)
            {
                EditorGUILayout.LabelField("Please select the mesh filter on the collision mesh");
                EditorGUILayout.LabelField("If you name the child mesh 'EnumsMesh' this will be done automatically");
                EditorGUILayout.BeginHorizontal();
                this.meshFilter = (MeshFilter)EditorGUILayout.ObjectField(this.meshFilter, typeof(MeshFilter), true);
                EditorGUILayout.EndHorizontal();

                if (meshFilter != null && dataLoaded == false)
                {
                    GetSplineMeshData();
                    GetMeshData();
                }
            }
            else
            {
                EditorGUIUtility.labelWidth = 170;
                EditorGUILayout.Space();
                this.selectedEnum = (MeshEnumsStaticData.MeshEnum)EditorGUILayout.EnumPopup("Selected Enum", selectedEnum);

                EditorGUILayout.Space();
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Selected Enum Color");
                EditorGUI.DrawRect(new Rect(175.0f, 35.0f, 140.0f, 20.0f), MeshEnumsStaticData.MeshEnumColors[selectedEnum]);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();
                if (GUILayout.Button("Reset enums back to standard"))
                {
                    if (EditorUtility.DisplayDialog("Reset enums back to standard", "Are you sure you want to reset the mesh enums back to standard", "Yes", "No"))
                    {
                        splineMeshData.vertexEnums = new int[meshColors.Length];
                        UpdateAllColors();
                        EditorUtility.SetDirty(splineMeshData);
                    }
                }

                EditorGUILayout.Space();
                if (GUILayout.Button("Set all mesh enums to selected enum"))
                {
                    if (EditorUtility.DisplayDialog("Set all mesh enums to selected enum", "Are you sure you want to Set all mesh enums to the selected enum", "Yes", "No"))
                    {
                        for (int i = 0; i < splineMeshData.vertexEnums.Length; ++i)
                        {
                            splineMeshData.vertexEnums[i] = (int)selectedEnum;
                        }

                        UpdateAllColors();
                        EditorUtility.SetDirty(splineMeshData);
                    }
                }
            }
        }

        private void OnSceneGUI(SceneView sceneView)
        {
            if (splineMeshData == null || meshFilter == null || mesh == null)
            {
                return;
            }

            Event e = Event.current;

            if (e.type == EventType.MouseDown)
            {
                mouseHeld = true;
            }

            if (e.type == EventType.MouseUp)
            {
                mouseHeld = false;
                EditorUtility.SetDirty(splineMeshData);
            }

            if (e.alt)
            {
                mouseHeld = false;
            }

            if (mouseHeld)
            {
                Ray ray = HandleUtility.GUIPointToWorldRay(e.mousePosition);
                if (splineMeshData.gameObject.scene.GetPhysicsScene().Raycast(ray.origin, ray.direction, out RaycastHit hit))
                {
                    int index = hit.triangleIndex * 3;
                    if (meshTriangles.Length >= index + 2)
                    {
                        int vertexEnum = (int)selectedEnum;
                        splineMeshData.vertexEnums[meshTriangles[index]] = vertexEnum;
                        splineMeshData.vertexEnums[meshTriangles[index + 1]] = vertexEnum;
                        splineMeshData.vertexEnums[meshTriangles[index + 2]] = vertexEnum;

                        if (MeshEnumsStaticData.MeshEnumColors.TryGetValue(selectedEnum, out Color color))
                        {
                            meshColors[meshTriangles[index]] = color;
                            meshColors[meshTriangles[index + 1]] = color;
                            meshColors[meshTriangles[index + 2]] = color;
                        }
                    }
                }
            }

            mesh.colors = meshColors;

            if (e.type == EventType.Layout)
            {
                HandleUtility.AddDefaultControl(GUIUtility.GetControlID(GetHashCode(), FocusType.Passive));
            }
        }

        private void UpdateAllColors()
        {
            for (int i = 0; i < meshColors.Length; ++i)
            {
                meshColors[i] = MeshEnumsStaticData.MeshEnumColors[(MeshEnumsStaticData.MeshEnum)splineMeshData.vertexEnums[i]];
            }

            mesh.colors = meshColors;
        }
    }
}