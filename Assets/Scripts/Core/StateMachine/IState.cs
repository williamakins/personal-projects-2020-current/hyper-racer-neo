﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState
{
    List<ITransition> transitions { get; set; }
    void Enter();
    void Update();
    void FixedUpdate();
    void LateUpdate();
    void Exit();
    void OnDestroy();
}
