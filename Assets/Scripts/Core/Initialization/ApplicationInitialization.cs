﻿using System.Collections;
using HyperRacer.Helper;
using LudumDare.Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HyperRacer.Core.Initialization
{
    [DisallowMultipleComponent]
    public sealed class ApplicationInitialization : MonoBehaviour
    {
        //defined as 0 as we always expect the boot scene to be 0
        private const int bootSceneIndex = 0;
        private const string mainSceneName = "Main";

        private const int targetFrameRate = 60;

        private void Awake()
        {
            InitializeModels();
            ConfigureSettings();

            SceneManagerHelper.Initialize();
            SceneManagerHelper.LoadLoadingScene();

            StartCoroutine(StartGame());
        }

        private void InitializeModels()
        {
            Models.ClearModels();
            Models.InitialiseModels();
        }

        private void ConfigureSettings()
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = targetFrameRate;
        }

        private IEnumerator StartGame()
        {
            yield return null;

            SceneManagerHelper.UnloadScene(bootSceneIndex);
            SceneManagerHelper.LoadSceneAsync(mainSceneName, LoadSceneMode.Additive);
        }
    }
}