using UnityEngine;

namespace HyperRacer.Core.Initialization
{
    public static class BootStrapper
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        public static void Initialize()
        {
            Debug.Log("BootStrapper :: Performing game initialization");

            ServiceLocator.ServiceLocator.Initialize();
        }
    }
}