using NaughtyAttributes;
using UnityEngine;

namespace HyperRacer.Core.ServiceLocator
{
    public sealed class InitializeServices : MonoBehaviour
    {
        [InfoBox("The order of the array defines the order these services will initialize")]
        [SerializeField]
        GameObject[] gameObjects = null;

        private void Awake()
        {
            if (ServiceLocator.Current == null)
            {
                Debug.LogError("InitializeServices :: ServiceLocator is null so services cannot initialize");
                return;
            }

            if (gameObjects?.Length > 0)
            {
                for (int i = 0; i < gameObjects.Length; i++)
                {
                    DontDestroyOnLoad(Instantiate(gameObjects[i], Vector3.zero, Quaternion.identity));
                }
            }
        }
    }
}