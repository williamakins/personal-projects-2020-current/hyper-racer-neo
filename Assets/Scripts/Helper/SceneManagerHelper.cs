﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HyperRacer.Helper
{
    public static class SceneManagerHelper
    {
        private static Dictionary<Scene, bool> LoadedScenes = new Dictionary<Scene, bool>();

        public static Dictionary<string, float> ScenesLoading = new Dictionary<string, float>();

        private const string loadingSceneName = "LoadingScene";
        private static GameObject loadingSceneRootObject = null;

        public static event Action<string, AsyncOperation> SceneLoadingBegun = (s, operation) => { };
        public static event Action AllLoadingComplete = () => { };

        public static void Initialize()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }

        public static void Uninitialize()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            SceneManager.sceneUnloaded -= OnSceneUnloaded;
        }

        public static void ShowHideAllLoadedScenes(bool show)
        {
            foreach (Scene s in LoadedScenes.Keys.ToList())
            {
                GameObject rootObject = FindRootObject(s);

                if (rootObject != null)
                {
                    rootObject.SetActive(show);

                    LoadedScenes[s] = show;
                }
            }
        }

        public static void TryShowHideScene(string sceneName, bool show)
        {
            foreach (KeyValuePair<Scene, bool> sceneStatus in LoadedScenes)
            {
                Scene scene = sceneStatus.Key;

                if (scene.name == sceneName && sceneStatus.Value == !show)
                {
                    GameObject rootObject = FindRootObject(scene);
                    if (rootObject != null)
                    {
                        rootObject.SetActive(show);

                        LoadedScenes[scene] = show;
                    }

                    return;
                }
            }

            Debug.LogWarning("Unable to show/hide scene " + sceneName + " as either the name is wrong or scene not loaded");
        }

        public static void TryShowHideScene(Scene scene, bool show)
        {
            if (LoadedScenes.ContainsKey(scene))
            {
                LoadedScenes[scene] = show;
            }
        }

        public static void LoadScene(string sceneName, LoadSceneMode loadSceneMode = LoadSceneMode.Additive, bool activeOnLoad = true, bool showLoadingScene = true)
        {
            if (showLoadingScene)
            {
                ShowLoadingScene();
            }

            SceneManager.LoadScene(sceneName, loadSceneMode);
            LoadedScenes.TryAdd(SceneManager.GetSceneByName(sceneName), activeOnLoad);
        }

        public static void LoadSceneAsync(string sceneName, LoadSceneMode loadSceneMode = LoadSceneMode.Additive, bool activeOnLoad = true, bool showLoadingScene = true)
        {
            if (showLoadingScene)
            {
                ShowLoadingScene();
            }

            AsyncOperation asyncOp = SceneManager.LoadSceneAsync(sceneName, loadSceneMode);
            LoadedScenes.TryAdd(SceneManager.GetSceneByName(sceneName), activeOnLoad);

            SceneLoadingBegun?.Invoke(sceneName, asyncOp);

            if (ScenesLoading.ContainsKey(sceneName))
            {
                ScenesLoading.Remove(sceneName);
            }
            ScenesLoading.Add(sceneName, 0.0f);
        }

        public static void UnloadScene(string sceneName, UnloadSceneOptions unloadSceneOptions = UnloadSceneOptions.None)
        {
            AsyncOperation asyncOp = SceneManager.UnloadSceneAsync(sceneName, unloadSceneOptions);
        }

        public static void UnloadScene(int sceneIndex, UnloadSceneOptions unloadSceneOptions = UnloadSceneOptions.None)
        {
            AsyncOperation asyncOp = SceneManager.UnloadSceneAsync(sceneIndex, unloadSceneOptions);
        }

        private static void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            //perform a check to ensure the scene is in the correct state on load
            GameObject rootObject = FindRootObject(scene);

            if (LoadedScenes.ContainsKey(scene))
            {
                if (rootObject.activeSelf != LoadedScenes[scene])
                {
                    rootObject.SetActive(LoadedScenes[scene]);
                }

                TryShowHideScene(scene, false);
            }

            if (ScenesLoading.ContainsKey(scene.name))
            {
                ScenesLoading.Remove(scene.name);
            }

            if (scene.name != loadingSceneName)
            {
                if (ScenesLoading.Count == 0)
                {
                    SceneManager.SetActiveScene(scene);

                    AllLoadingComplete?.Invoke();
                }
            }
            else
            {
                //get the root object of the loading scene
                FindLoadingSceneRootObject();
            }
        }

        private static void OnSceneUnloaded(Scene scene)
        {
            LoadedScenes.TryRemove(scene);
        }

        private static void TryAdd(this Dictionary<Scene, bool> dictionary, Scene key, bool value)
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, value);
            }
            else
            {
                Debug.LogWarning("Warning: cannot add scene " + key.name + " to dict as it already exists");
            }
        }

        private static void TryRemove(this Dictionary<Scene, bool> dictionary, Scene key)
        {
            if (dictionary.ContainsKey(key))
            {
                dictionary.Remove(key);
            }
        }

        private static GameObject FindRootObject(Scene scene)
        {
            GameObject[] rootObjects = scene.GetRootGameObjects();
            if (rootObjects != null && rootObjects.Length > 0)
            {
                return rootObjects[0];
            }

            return null;
        }

        public static void LoadLoadingScene()
        {
            if (loadingSceneRootObject == null)
            {
                SceneManager.LoadSceneAsync(loadingSceneName, LoadSceneMode.Additive);
            }
            else
            {
                Debug.LogWarning("Error: loading scene is already loaded!");
            }
        }

        private static void FindLoadingSceneRootObject()
        {
            Scene loadingScene = SceneManager.GetSceneByName(loadingSceneName);
            GameObject rootObject = FindRootObject(loadingScene);

            if (rootObject != null)
            {
                loadingSceneRootObject = rootObject;
            }
            else
            {
                Debug.LogWarning("Error: Can't find loading scene root object");
            }
        }

        private static void ShowLoadingScene()
        {

            if (loadingSceneRootObject != null)
            {
                loadingSceneRootObject.SetActive(true);
            }
        }

        public static void HideLoadingScene()
        {
            if (loadingSceneRootObject != null)
            {
                loadingSceneRootObject.SetActive(false);
            }
        }
    }
}