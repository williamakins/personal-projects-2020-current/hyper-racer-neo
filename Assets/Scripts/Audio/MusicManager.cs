﻿using System;
using UnityEngine;
using LudumDare.Core;
using LudumDare.Core.EventManager;
using LudumDare.Model;
using HyperRacer.Core.ServiceLocator;

public class MusicManager : MonoBehaviour
{
    [Serializable]
    struct ClipDetails
    {
        public MusicClip clip;
        public int minLap;
        public int maxLap;
    }
    [SerializeField]
    private float timer = 4.0f;


    [SerializeField]
    ClipDetails[] clips;

    [Header("Normal Music")]

    [SerializeField]
    private int lapToSwitchToNormalMusic = 8;

    [SerializeField]
    private AudioSource normalSource = null;

    [SerializeField]
    private AudioClip[] normalMusicClips;

    private int currentMusicIndex = 0;
    private int previousMusicIndex = -1;

    private GameModel gameModel;

    private AudioClip currentMainClip;

    private bool lapComplete = true;

    private bool normalMusic = false;

    private float currentTime;
    private int currentLap = 1;

    private float normalMusicCounter;

    private IEventManager<Events> eventManager = null;

    // Start is called before the first frame update
    void Start()
    {
        gameModel = Models.GetModel<GameModel>();
        eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();
        eventManager.RegisterEvent(Events.OnLap, OnLapUpdated);
    }

    private void OnDestroy()
    {
        eventManager.DeregisterEvent(Events.OnLap, OnLapUpdated);
    }

    // Update is called once per frame
    void Update()
    {
        if (!normalMusic)
        {
            if (currentLap >= lapToSwitchToNormalMusic)
            {
                normalMusic = true;
            }
            timer -= Time.deltaTime;
            if (currentTime <= 0.0f)
            {
                timer = currentTime;
                if (lapComplete)
                {
                    lapComplete = false;
                    ProcessClips();
                }
            }
        }
        else
        {
            normalMusicCounter -= Time.deltaTime;
            if(normalMusicCounter <= 0)
            {
                SetNormalMusic();
            }
        }
    }

    private void SetNormalMusic()
    {
        if(normalMusicClips.Length > 0 && currentMusicIndex < normalMusicClips.Length)
        {
            do
            {
                currentMusicIndex = UnityEngine.Random.Range(0, normalMusicClips.Length - 1);
            } 
            while (currentMusicIndex == previousMusicIndex);

            previousMusicIndex = currentMusicIndex;

            //quick fix for sound overlapping bug
            for (int i = 0; i < clips.Length; ++i)
            {
                clips[i].clip.Source.Stop();
            }
            normalSource.Stop();

            AudioClip newClip = normalMusicClips[currentMusicIndex];
            normalMusicCounter = newClip.length + 0.5f;
            normalSource.PlayOneShot(newClip);
            currentMainClip = newClip;
            normalSource.volume = 0.7f;
        }
    }

    private void OnLapUpdated(Events arg1, object[] arg2)
    {
        currentLap = gameModel.lap;
        lapComplete = true;
    }

    private void ProcessClips()
    {

        for (int i = 0; i < clips.Length; i++)
        {
            if ((clips[i].maxLap != 0 && clips[i].maxLap <= currentLap) || currentLap >= lapToSwitchToNormalMusic)
            {
                if (currentMainClip == clips[i].clip.Source.clip)
                {
                    currentMainClip = null;
                }
                clips[i].clip.Stop();
            }
            else if (clips[i].minLap <= currentLap)
            {
                clips[i].clip.Play();
                if (currentMainClip == null)
                {
                    currentMainClip = clips[i].clip.Source.clip;
                }
                else if (clips[i].clip.ListenToVolume)
                {
                    currentMainClip = clips[i].clip.Source.clip;
                }
            }
        }

    }
}
