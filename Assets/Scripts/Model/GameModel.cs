﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LudumDare.Model
{
    public class GameModel : AbstractModel
    {
        public class PositionalData
        {
            public PositionalData(Vector3 position, Vector3 rotation)
            {
                this.position = position;
                this.rotation = rotation;
            }
            Vector3 position;
            Vector3 rotation;
            public PositionalData next = null;

            public Vector3 Position { get => position;}
            public Vector3 Rotation { get => rotation; }
        }

        public PositionalData first = null;
        private PositionalData last = null;

        public void AddToList(Vector3 position, Vector3 rotation)
        {
            PositionalData newData = new PositionalData(position, rotation);
            if (first == null)
            {
                first = newData;
                last = newData;
            }
            else
            {
                last.next = newData;
                last = newData;
            }
            OnPositionUpdated?.Invoke(position);
        }

        public void ResetPosData()
        {
            if (first != null)
            {
                first = null;
                last = null;
            }
        }

        private int m_lap = 1;
        public int lap
        {
            get => m_lap;

            set
            {
                m_lap = value;
                //OnLapUpdated?.Invoke(m_lap);
            }
        }

        //public event Action<int> OnLapUpdated;

        public event Action<Vector3> OnPositionUpdated;

        private string m_level = "";

        public string level
        {
            get => m_level;

            set
            {
                m_level = value;
                OnLevelUpdated?.Invoke(m_lap);
            }
        }

        public event Action<int> OnLevelUpdated;

        private bool m_carFrozen = true;
        public bool carFrozen
        {
            get => m_carFrozen;

            set
            {
                m_carFrozen = value;
                OnCarFreezeUpdated?.Invoke(m_carFrozen);
            }
        }

        public event Action<bool> OnCarFreezeUpdated;

        public List<GameObject> ghostCars = new List<GameObject>();

        //public struct GhostCars
        //{
        //    private List<GameObject> m_ghostCars;

        //    public List<GameObject> Get()
        //    {
        //        return m_ghostCars;
        //    }

        //    public void Set(List<GameObject> newGhostCars)
        //    {
        //        m_ghostCars = newGhostCars;
        //    }

        //    public void Add(GameObject car)
        //    {
        //        m_ghostCars.Add(car);
        //    }

        //    public void Reset()
        //    {
        //        m_ghostCars.Clear();
        //    }

        //    public void Init()
        //    {
        //        if (m_ghostCars != null && m_ghostCars.Count > 0)
        //        {
        //            m_ghostCars.Clear();
        //        }

        //        m_ghostCars = new List<GameObject>();
        //    }
        //}

        //public GhostCars ghostCars = new GhostCars();
    }
}