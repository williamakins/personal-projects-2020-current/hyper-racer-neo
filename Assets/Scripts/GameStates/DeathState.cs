﻿using LudumDare.Core.EventManager;
using LudumDare.Model;
using System.Collections;
using System.Collections.Generic;
using HyperRacer.Helper;
using UnityEngine;
using HyperRacer.Core.ServiceLocator;

public class DeathState : IState
{
    public List<ITransition> transitions { get; set; }

    private float timer = 0.0f;
    private static readonly int Vector1Dbedc6Cb = Shader.PropertyToID("Vector1_DBEDC6CB");
    private static readonly int Vector1Fb7C3835 = Shader.PropertyToID("Vector1_FB7C3835");

    private Transform carTransform = null;
    private Rigidbody carRigidbody = null;
    private Material carMat = null;
    private CarModel carModel = null;

    private float deathTime = 0.3f;
    private float delaySecs = 3.0f;

    private bool shouldResetMap = false;

    private IEventManager<Events> eventManager;
    public DeathState(Rigidbody rb, Transform transform, CarModel carModel, Material carMat)
    {
        transitions = new List<ITransition>();

        this.carTransform = transform;
        this.carRigidbody = rb;
        this.carModel = carModel;
        this.carMat = carMat;

        eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();
    }

    public void Enter()
    {
        eventManager.RegisterEvent(Events.ResetMap, OnResetMap);
        eventManager.RegisterEvent(Events.FailConditionFinished, FailConditionFinished);

        InitStateData();
    }

    private void InitStateData()
    {
        shouldResetMap = false;
        timer = 0.0f;

        carRigidbody.Sleep();
    }

    public void Exit()
    {
        eventManager.DeregisterEvent(Events.ResetMap, OnResetMap);
        eventManager.DeregisterEvent(Events.FailConditionFinished, FailConditionFinished);

        carRigidbody.WakeUp();
    }

    public void OnDestroy()
    {
    }

    public void Update()
    {
        float dt = Time.deltaTime;
        timer += dt;
          
        LerpCarOnDeath(dt);

        if (timer >= delaySecs)
        {
            eventManager.TriggerEvent(Events.FailConditionFinished);
        }
    }

    public void FixedUpdate()
    {

    }

    public void LateUpdate() {}

    private void FailConditionFinished(Events arg1, object[] arg2)
    {
        SceneManagerHelper.ShowHideAllLoadedScenes(false);
        SceneManagerHelper.TryShowHideScene("EndScene", true);
    }

    private void OnResetMap(Events arg1, object[] arg2)
    {
        shouldResetMap = true;
    }

    private void LerpCarOnDeath(float dt)
    {
        carTransform.position += Vector3.down * dt;

        //Sort of a hack having this in this in the view script, but fuck it and the hard coded 3 seconds
        //carModel.currentSpeed = Mathf.Lerp(carModel.currentSpeed, 0.0f, timer/deathTime);

        //carMat.SetFloat(Vector1Dbedc6Cb, timer / deathTime);
        //carMat.SetFloat(Vector1Fb7C3835, 5000.0f);
    }

    public bool IsDead()
    {
        //return timer >= deathTime;

        return shouldResetMap;
    }
}
