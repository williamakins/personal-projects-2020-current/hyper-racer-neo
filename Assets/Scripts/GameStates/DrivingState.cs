﻿using LudumDare.Controller;
using LudumDare.Core;
using LudumDare.Core.EventManager;
using LudumDare.Model;
using System.Collections.Generic;
using HyperRacer.Editor.MeshEnums;
using UnityEngine;
using static HyperRacer.Editor.MeshEnums.MeshEnumsStaticData;
using HyperRacer.Core.ServiceLocator;

public class DrivingState : IState
{
    public List<ITransition> transitions { get; set; }

    private CarModel carModel = null;
    private GameModel gameModel = null;

    private Rigidbody carRigidbody = null;
    private Transform rayOriginPoint = null;
    private Transform transform;
    private LayerMask mask;

    private Quaternion desiredRot = Quaternion.identity;

    private LeadCarPositionsController positionPlacementController;

    private float minDistance = 0.1f;
    private float fallSpeed = 2.0f;
    private RaycastHit hit = new RaycastHit();

    private int savedInstanceId = -1;

    private Mesh mesh = null;
    private MeshEnums meshEnums = null;
    private Vector3[] normals = null;
    private int[] triangles = null;
    private MeshEnum currentMeshEnum = MeshEnum.Standard;

    private bool playerFailed = false;

    private Transform lastSuccessfulTransform = null;

    private Vector3 lastSucessfulNormal = Vector3.zero;
    private Vector3 lastSucessfulPosition = Vector3.zero;
    private IEventManager<Events> eventManager = null;

    //private bool deathConditionMet = false;
    //private bool mapExited = false;
    public DrivingState(Rigidbody carRigidbody, Transform rayOriginPoint, Transform transform, LayerMask mask, LeadCarPositionsController leadCarPositionsController)
    {
        transitions = new List<ITransition>();
        positionPlacementController = leadCarPositionsController;

        eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();
        carModel = Models.GetModel<CarModel>();
        gameModel = Models.GetModel<GameModel>();

        this.carRigidbody = carRigidbody;
        this.rayOriginPoint = rayOriginPoint;
        this.transform = transform;
        this.mask = mask;
        this.minDistance = carModel.minGroundDistance;
        this.fallSpeed = carModel.fallSpeed;
    }

    public void Enter()
    {
        eventManager.RegisterEvent(Events.FailConditionMet, OnDeathConditionMet);
        //eventManager.RegisterEvent(Events.OnMapExit, OnMapExit);
        carModel.OnTurningUpdated += OnTurningUpdated;
        gameModel.carFrozen = false;

        InitStateData();
    }

    private void InitStateData()
    {
        playerFailed = false;
        savedInstanceId = -1;
        currentMeshEnum = MeshEnum.Standard;
    }

    //private void OnMapExit(Events arg1, object[] arg2)
    //{
    //    mapExited = true;
    //}

    private void OnDeathConditionMet(Events arg1, object[] arg2)
    {
        //deathConditionMet = true;
    }

    public void Exit()
    {
        eventManager.DeregisterEvent(Events.FailConditionMet, OnDeathConditionMet);
        //eventManager.DeregisterEvent(Events.OnMapExit, OnMapExit);
        carModel.OnTurningUpdated -= OnTurningUpdated;
        gameModel.carFrozen = true;
    }

    public void OnDestroy()
    {
    }

    public void Update()
    {

    }

    public void FixedUpdate()
    {
        if (playerFailed)
        {
            return;
        }

        //if (!Physics.CapsuleCast(transform.position, transform.position +- transform.up * 2, 1.0f, -transform.up, 100.0f, mask, QueryTriggerInteraction.Ignore))
        //{

        //}

        //Gizmos.DrawSphere(transform.position, 0.01f);

        Ray downRay = new Ray(rayOriginPoint.position, -transform.up);
        //if (!Physics.SphereCast(rayOriginPoint.position, 0.03f, -transform.up, out hit, 100, mask, QueryTriggerInteraction.Ignore))
        if (!Physics.Raycast(downRay, out hit, 100, mask, QueryTriggerInteraction.Ignore))
        {
            Debug.Log("oooo");

            //Collider[] hitColliders = Physics.OverlapSphere(transform.position, 5.0f, mask, QueryTriggerInteraction.Ignore);
            //foreach (var hitCollider in hitColliders)
            //{
            //    Debug.Log(hitCollider.name);

            //    if (hitCollider.name == "collisionMesh")
            //    {
            //        transform.position = hitCollider.ClosestPointOnBounds(transform.position);
            //    }
            //}

            //transform = lastSuccessfulTransform;

            //return;

            Vector3 dir = (transform.position - lastSucessfulNormal).normalized;
            Ray upRay = new Ray(transform.position, dir);

            if (!Physics.Raycast(upRay, out hit, 100, mask, QueryTriggerInteraction.Ignore))
            {
                Debug.Log("oh shit! It's time to die...");

                if (!playerFailed)
                {
                    playerFailed = true;
                    eventManager.TriggerEvent(Events.FailConditionMet);
                }

                return;
            }


            //Vector3 dir = (transform.position - lastSucessfulNormal).normalized;

            //Ray returnRay = new Ray(transform.position, dir);
            //if (!Physics.Raycast(returnRay, out hit, 100, mask, QueryTriggerInteraction.Ignore))
            //{
            //    Debug.Log("fuck");

            //    return;
            //}

            //Debug.DrawRay(transform.position, lastSucessfulNormal);


            //Debug.Log("raycast failed");

            //Ray backRay = new Ray(transform.position, -transform.forward);
            //if (!Physics.Raycast(backRay, out hit, 100, mask, QueryTriggerInteraction.Ignore))
            //{
            //    Debug.Log("yikes! take me back!!!!");

            //    Ray upRay = new Ray(transform.position, transform.up);
            //    if (!Physics.Raycast(upRay, out hit, 100, mask, QueryTriggerInteraction.Ignore))
            //    {
            //        Debug.Log("omg whaaaaaaaaaa");

            //        Ray forwardRay = new Ray(transform.position, transform.forward);
            //        if (!Physics.Raycast(upRay, out hit, 100, mask, QueryTriggerInteraction.Ignore))
            //        {
            //            Debug.Log("my god wtf");
            //            return;
            //        }
            //    }
            //}
            //return;



            //transform = lastSuccessfulTransform;

            //Ray forwardRay = new Ray(transform.position, -transform.forward);
            //if (!Physics.Raycast(forwardRay, out RaycastHit forwardHit, 100, mask, QueryTriggerInteraction.Ignore))
            //{
            //    return;
            //}

            //UpdateNormalRotation(forwardHit);

            //Vector3 futureTransform = transform.position + (transform.forward * (carModel.currentSpeed * Time.deltaTime));
            //if (Physics.Linecast(futureTransform, transform.position, mask))
            //{
            //    Ray ray = new Ray(futureTransform, transform.up);
            //    if (!Physics.Raycast(ray, out var newHit, 100, mask, QueryTriggerInteraction.Ignore))
            //    {
            //        futureTransform = transform.position;
            //        return;
            //    }
            //    else
            //    {
            //        futureTransform = newHit.point + (transform.up);
            //    }
            //}
            //transform.position = futureTransform;

            //return;
        }

        

        //Ray forwardRay = new Ray(transform.position, transform.forward);
        //if (!Physics.Raycast(forwardRay, out RaycastHit forwardHit, 100, mask, QueryTriggerInteraction.Ignore))
        //{
        //    return;
        //}
        UpdateNormalRotation();
        UpdateForwardMovement();

        //UpdateNormalRotation(forwardHit);


        //UpdateFalling();
        //lastSuccessfulTransform = transform;

        positionPlacementController.ProcessPositions();
    }

    public void LateUpdate()
    {

    }

    private void UpdateNormalRotation()
    {
        // Just in case, also make sure the collider also has a renderer, material and texture
        MeshCollider meshCollider = hit.collider as MeshCollider;
        if (meshCollider == null || meshCollider.sharedMesh == null)
        {
            return;
        }

        //update the saved data as we're on a new mesh segment
        int curInstanceId = hit.collider.GetInstanceID();
        if (savedInstanceId != curInstanceId)
        {
            savedInstanceId = curInstanceId;

            if (hit.collider.TryGetComponent(out MeshEnums meshEnums))
            {
                this.meshEnums = meshEnums;
            }

            mesh = meshCollider.sharedMesh;
            normals = mesh.normals;
            triangles = mesh.triangles;
        }

        // Extract local space normals of the triangle we hit
        int index = hit.triangleIndex * 3;
        Vector3 n0 = normals[triangles[index]];
        Vector3 n1 = normals[triangles[index + 1]];
        Vector3 n2 = normals[triangles[index + 2]];

        // Use barycentric coordinate to calculate the interpolated normal
        Vector3 baryCenter = hit.barycentricCoordinate;
        Vector3 interpolatedNormal = n0 * baryCenter.x + n1 * baryCenter.y + n2 * baryCenter.z;
        interpolatedNormal = interpolatedNormal.normalized;
        interpolatedNormal = hit.collider.transform.TransformDirection(interpolatedNormal);

        lastSucessfulNormal = interpolatedNormal;

        Quaternion newRot = Quaternion.FromToRotation(transform.up, interpolatedNormal) * carRigidbody.rotation;
        if (!carRigidbody.rotation.Equals(newRot))
        {
            carRigidbody.rotation = newRot;
        }

        //only update if the mesh enum is different from the previous
        if (meshEnums != null)
        {
            MeshEnum newMeshEnum = (MeshEnum) meshEnums.vertexEnums[triangles[index]];

            if (currentMeshEnum != newMeshEnum)
            {
                HandleMeshEnum((MeshEnum)meshEnums.vertexEnums[triangles[index]]);
            }
        }
    }

    private void HandleMeshEnum(MeshEnum meshEnum)
    {
        //Debug.Log("enum = " + meshEnum);

        currentMeshEnum = meshEnum;

        switch (meshEnum)
        {
            case MeshEnum.Standard:
                break;
            case MeshEnum.Slow:
                break;
            case MeshEnum.Fast:
                break;
            case MeshEnum.Hole:
                if (!playerFailed)
                {
                    playerFailed = true;
                    eventManager.TriggerEvent(Events.FailConditionMet);
                }
                break;
            case MeshEnum.Lap:
                eventManager.TriggerEvent(Events.OnLap);
                break;
            default:
                break;
        }
    }

    public bool PlayerFailed()
    {
        return playerFailed;
    }

    public bool GamePaused()
    {
        return Time.timeScale == 0.0f;
    }

    private void UpdateForwardMovement()
    {
        //carRigidbody.MovePosition(transform.position + (transform.forward * (carModel.currentSpeed * Time.fixedDeltaTime)));
        transform.position += transform.forward * (carModel.currentSpeed * Time.fixedDeltaTime);
        //Vector3 futureTransform = transform.position + (transform.forward * (carModel.currentSpeed * Time.deltaTime));
        //if (Physics.Linecast(futureTransform, transform.position, mask))
        //{
        //    Ray ray = new Ray(futureTransform, transform.up);
        //    if (!Physics.Raycast(ray, out var newHit, 100, mask, QueryTriggerInteraction.Ignore))
        //    {
        //        futureTransform = transform.position;
        //        return;
        //    }
        //    else
        //    {
        //        futureTransform = newHit.point + (transform.up);
        //    }
        //}
        //transform.position = futureTransform;
    }

    private void UpdateFalling()
    {
        if (hit.distance >= carModel.minGroundDistance)
        {
            carRigidbody.position = Vector3.MoveTowards(transform.position, hit.point, carModel.fallSpeed * Time.deltaTime);
        }
    }

    private void OnTurningUpdated(float turning)
    {
        if (turning != 0.0f /*&& !PlayerDeath()*/)
        {
            carRigidbody.rotation *= Quaternion.AngleAxis(turning * carModel.rotationSpeed * Time.deltaTime, Vector3.up);
        }
    }
}
