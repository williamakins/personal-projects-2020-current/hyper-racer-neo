﻿using HyperRacer.Core.ServiceLocator;
using LudumDare.Core.EventManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingState : IState
{
    public List<ITransition> transitions { get; set; }

    private float duration = 0.0f;
    private float currentTime = 0.0f;
    private float nextNumber = 0.0f;
    private IEventManager<Events> eventManager;
    public StartingState(float startingDuration)
    {
        eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();
        transitions = new List<ITransition>();
        duration = startingDuration;
    }

    public void Enter()
    {
        InitStateData();

        currentTime = duration;
        nextNumber = duration;
    }

    private void InitStateData()
    {
        currentTime = 0.0f;
    }

    public void Exit()
    {
    }

    public void OnDestroy()
    {
    }

    public void Update()
    {
        currentTime -= Time.deltaTime;
        if(currentTime<= nextNumber)
        {
            eventManager.TriggerEvent(Events.StartCountdown, nextNumber);
            nextNumber -= 1.0f;
        }
    }

    public void FixedUpdate()
    {

    }

    public void LateUpdate() { }

    public bool TimeIsUp()
    {
        return currentTime <= 0.0f;
    }
}
