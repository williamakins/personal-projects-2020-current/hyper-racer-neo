﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LudumDare.Core;
using LudumDare.Core.EventManager;
using LudumDare.Model;
using HyperRacer.Core.ServiceLocator;

public class PlayerSettingUpdater : MonoBehaviour
{
    private CarModel carModel = null;

    [SerializeField]
    private float speedIncrease = 1.0f;

    [SerializeField]
    private float increaseRate = 1.0f;

    private float desiredSpeed = 0.0f;

    private IEventManager<Events> eventManager;
    private void Start()
    {
        carModel = Models.GetModel<CarModel>();

        eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();
        eventManager.RegisterEvent(Events.OnLap, OnLapUpdated);

        desiredSpeed = carModel.minSpeed;
    }

    private void OnLapUpdated(Events arg1, object[] arg2)
    {
        if(carModel.currentSpeed < carModel.maxSpeed)
        {
            desiredSpeed += speedIncrease;
        }
    }

    private void Update()
    {
        if (carModel != null && carModel.currentSpeed < carModel.maxSpeed && carModel.currentSpeed < desiredSpeed )
        {
            carModel.currentSpeed += Time.deltaTime * increaseRate;
        }
    }

    private void OnDestroy()
    {
        eventManager.DeregisterEvent(Events.OnLap, OnLapUpdated);
    }
}
