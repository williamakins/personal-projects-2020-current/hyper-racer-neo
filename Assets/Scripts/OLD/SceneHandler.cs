﻿using LudumDare.Core.EventManager;
using LudumDare.Model;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LudumDare.Core
{
    public class SceneHandler : MonoBehaviour
    {
        GameModel gameModel = null;
        private void Awake()
        {
            LoadScenes();
            //eventManager.RegisterEvent(Events.FailConditionMet, FailConditionMet);
        }

        private void Start()
        {
            gameModel = Models.GetModel<GameModel>();
            //gameModel.level =  SceneManager.GetActiveScene().name;
        }

        //private void FailConditionMet(Events arg1, object[] arg2)
        //{
        //    SceneManager.LoadScene("EndScene");
        //}

        private void LoadScenes()
        {
            bool hudSceneLoaded = false;
            bool musicSceneLoaded = false;

            int countLoaded = SceneManager.sceneCount;
            Scene[] loadedScenes = new Scene[countLoaded];

            for (int i = 0; i < countLoaded; i++)
            {
                loadedScenes[i] = SceneManager.GetSceneAt(i);

                if (loadedScenes[i].name == "HUDScene")
                {
                    hudSceneLoaded = true;
                }

                if (loadedScenes[i].name == "MusicScene")
                {
                    musicSceneLoaded = true;
                }
            }

            if (hudSceneLoaded == false)
            {
                //SceneManager.LoadScene("HUDScene", LoadSceneMode.Additive);
            }

            if (musicSceneLoaded == false)
            {
                //SceneManager.LoadScene("MusicScene", LoadSceneMode.Additive);
            }
        }

        //private void OnDestroy()
        //{
        //    eventManager.DeregisterEvent(Events.FailConditionMet, FailConditionMet);
        //}
    }
}