﻿using HyperRacer.Core.ServiceLocator;
using HyperRacer.Helper;
using LudumDare.Core.EventManager;
using UnityEngine;

public class FailConditionListener : MonoBehaviour
{
    [SerializeField] private float delaySecs = 3.0f;

    private bool endGameStarted = false;
    private float timer = 0.0f;

    IEventManager<Events> eventManager = null;
    private void Awake()
    {
        eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();
        eventManager.RegisterEvent(Events.FailConditionMet, FailConditionMet);
        eventManager.RegisterEvent(Events.FailConditionFinished, FailConditionFinished);
    }

    private void FailConditionMet(Events arg1, object[] arg2)
    {
        endGameStarted = true;
    }

    private void FailConditionFinished(Events arg1, object[] arg2)
    {
        endGameStarted = false;

        //SceneManagerHelper.ShowHideAllLoadedScenes(false);
        //SceneManagerHelper.TryShowHideScene("EndScene", true);
    }

    private void Update()
    {
        if (endGameStarted == true)
        {
            timer += Time.deltaTime;

            if (timer >= delaySecs)
            {
                //eventManager.TriggerEvent(Events.FailConditionFinished);
            }
        }
    }

    private void OnDestroy()
    {
        eventManager.DeregisterEvent(Events.FailConditionMet, FailConditionMet);
        eventManager.DeregisterEvent(Events.FailConditionFinished, FailConditionFinished);
    }
}