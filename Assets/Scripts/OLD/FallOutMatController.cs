﻿using System.Collections;
using System.Collections.Generic;
using HyperRacer.Core.ServiceLocator;
using LudumDare.Core.EventManager;
using UnityEngine;

public class FallOutMatController : MonoBehaviour
{
    private IEventManager<Events> eventManager;
    // Start is called before the first frame update
    void Start()
    {

        eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            eventManager.TriggerEvent(Events.OnMapExit);
        }
    }
}
