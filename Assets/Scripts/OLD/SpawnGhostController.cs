﻿using LudumDare.Core;
using LudumDare.Core.EventManager;
using LudumDare.Model;
using UnityEngine;

namespace LudumDare.Controller
{
    public class SpawnGhostController : AbstractController
    {
        //[SerializeField] private GameObject objectToSpawn;

        private GameModel gameModel = null;

        protected override void Awake()
        {
            //gameModel = Models.GetModel<GameModel>();
        }

        protected override void Start()
        {
            base.Start();

            //eventManager.RegisterEvent(Events.OnLap, OnLapUpdated);
        }

        private void OnLapUpdated(Events arg1, object[] arg2)
        {
            //if (objectToSpawn != null)
            //{
            //    //GameObject car = Instantiate(objectToSpawn);
            //    //Debug.Log("Instantiate");
            //    //gameModel.ghostCars.Add(car);
            //}
            //else
            //{
            //    Debug.LogWarning("Cannot instantiate ghost, game object is null");
            //}
        }

        protected override void OnDestroy()
        {
            //eventManager.RegisterEvent(Events.OnLap, OnLapUpdated);
        }
    }
}