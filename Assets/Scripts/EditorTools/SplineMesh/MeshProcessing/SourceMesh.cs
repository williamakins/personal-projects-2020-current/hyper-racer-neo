﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

namespace SplineMesh {
    /// <summary>
    /// This class returns a transformed version of a given source mesh, plus others
    /// informations to help bending the mesh along a curve.
    /// It is imutable to ensure better performances.
    /// 
    /// To obtain an instance, call the static method <see cref="Build(Mesh)"/>.
    /// The building is made in a fluent way.
    /// </summary>
    public struct SourceMesh {
        private Vector3 translation;
        private Quaternion rotation;
        private Vector3 scale;

        private NativeArray<Vector3> meshVertices;
        private NativeArray<Vector3> meshNormals;
        private NativeArray<MeshVertexStruct> meshVertexStruct;

        private BuildDataJob buildDataJob;
        private JobHandle buildDataJobHandle;

        internal Mesh Mesh { get; }

        private List<MeshVertexStruct> vertices;
        internal List<MeshVertexStruct> Vertices {
            get {
                if (vertices == null) BeginBuildDataJob();
                return vertices;
            }
        }

        private int[] triangles;
        internal int[] Triangles {
            get {
                if (vertices == null) BeginBuildDataJob();
                return triangles;
            }
        }

        private float minX;
        internal float MinX {
            get {
                if (vertices == null) BeginBuildDataJob();
                return minX;
            }
        }

        private float length;
        internal float Length {
            get {
                if (vertices == null) BeginBuildDataJob();
                return length;
            }
        }

        /// <summary>
        /// constructor is private to enable fluent builder pattern.
        /// Use <see cref="Build(Mesh)"/> to obtain an instance.
        /// </summary>
        /// <param name="mesh"></param>
        private SourceMesh(Mesh mesh) {
            Mesh = mesh;
            translation = default(Vector3);
            rotation = default(Quaternion);
            scale = default(Vector3);
            vertices = null;
            triangles = null;
            minX = 0;
            length = 0;

            meshVertices = new NativeArray<Vector3>();
            meshNormals = new NativeArray<Vector3>();
            meshVertexStruct = new NativeArray<MeshVertexStruct>();

            buildDataJob = new BuildDataJob()
            {
                translation = this.translation,
                rotation = this.rotation,
                scale = this.scale
            };
            buildDataJobHandle = new JobHandle();
        }

        /// <summary>
        /// copy constructor
        /// </summary>
        /// <param name="other"></param>
        private SourceMesh(SourceMesh other) {
            Mesh = other.Mesh;
            translation = other.translation;
            rotation = other.rotation;
            scale = other.scale;
            vertices = null;
            triangles = null;
            minX = 0;
            length = 0;

            meshVertices = new NativeArray<Vector3>();
            meshNormals = new NativeArray<Vector3>();
            meshVertexStruct = new NativeArray<MeshVertexStruct>();

            buildDataJob = new BuildDataJob()
            {
                translation = this.translation,
                rotation = this.rotation,
                scale = this.scale
            };
            buildDataJobHandle = new JobHandle();
        }

        public static SourceMesh Build(Mesh mesh) {
            return new SourceMesh(mesh);
        }

        public SourceMesh Translate(Vector3 translation) {
            var res = new SourceMesh(this) {
                translation = translation
            };
            return res;
        }

        public SourceMesh Translate(float x, float y, float z) {
            return Translate(new Vector3(x, y, z));
        }

        public SourceMesh Rotate(Quaternion rotation) {
            var res = new SourceMesh(this) {
                rotation = rotation
            };
            return res;
        }

        public SourceMesh Scale(Vector3 scale) {
            var res = new SourceMesh(this) {
                scale = scale
            };
            return res;
        }

        public SourceMesh Scale(float x, float y, float z) {
            return Scale(new Vector3(x, y, z));
        }

        //private void BuildData() {
        //    // if the mesh is reversed by scale, we must change the culling of the faces by inversing all triangles.
        //    // the mesh is reverse only if the number of resersing axes is impair.
        //    bool reversed = scale.x < 0;
        //    if (scale.y < 0) reversed = !reversed;
        //    if (scale.z < 0) reversed = !reversed;
        //    triangles = reversed ? MeshUtility.GetReversedTriangles(Mesh) : Mesh.triangles;

        //    // we transform the source mesh vertices according to rotation/translation/scale
        //    int i = 0;
        //    vertices = new List<MeshVertexStruct>(Mesh.vertexCount);
        //    foreach (Vector3 vert in Mesh.vertices) {
        //        var transformed = new MeshVertexStruct(vert, Mesh.normals[i++]);
        //        //  application of rotation
        //        if (rotation != Quaternion.identity) {
        //            transformed.position = rotation * transformed.position;
        //            transformed.normal = rotation * transformed.normal;
        //        }
        //        if (scale != Vector3.one) {
        //            transformed.position = Vector3.Scale(transformed.position, scale);
        //            transformed.normal = Vector3.Scale(transformed.normal, scale);
        //        }
        //        if (translation != Vector3.zero) {
        //            transformed.position += translation;
        //        }
        //        vertices.Add(transformed);
        //    }

        //    // find the bounds along x
        //    minX = float.MaxValue;
        //    float maxX = float.MinValue;
        //    foreach (var vert in vertices)
        //    {
        //        Vector3 p = vert.position;
        //        maxX = Math.Max(maxX, p.x);
        //        minX = Math.Min(minX, p.x);
        //    }
        //    length = Math.Abs(maxX - minX);
        //}

        private void BeginBuildDataJob()
        {
            // if the mesh is reversed by scale, we must change the culling of the faces by inversing all triangles.
            // the mesh is reverse only if the number of resersing axes is impair.
            bool reversed = scale.x < 0;
            if (scale.y < 0) reversed = !reversed;
            if (scale.z < 0) reversed = !reversed;
            triangles = reversed ? MeshUtility.GetReversedTriangles(Mesh) : Mesh.triangles;

            meshVertices = new NativeArray<Vector3>(Mesh.vertices, Allocator.Persistent);
            meshNormals = new NativeArray<Vector3>(Mesh.normals, Allocator.Persistent);
            meshVertexStruct = new NativeArray<MeshVertexStruct>(Mesh.vertices.Length, Allocator.Persistent);

            buildDataJob = new BuildDataJob()
            {
                translation = this.translation,
                rotation = this.rotation,
                scale = this.scale,

                meshVertices = this.meshVertices,
                meshNormals = this.meshNormals,
                meshVertexStruct = this.meshVertexStruct
            };

            buildDataJobHandle = buildDataJob.Schedule(Mesh.vertices.Length, 16);

            buildDataJobHandle.Complete();

            vertices = buildDataJob.meshVertexStruct.ToList();

            meshVertices.Dispose();
            meshNormals.Dispose();
            meshVertexStruct.Dispose();

            // find the bounds along x
            minX = float.MaxValue;
            float maxX = float.MinValue;
            foreach (var vert in vertices)
            {
                Vector3 p = vert.position;
                maxX = Math.Max(maxX, p.x);
                minX = Math.Min(minX, p.x);
            }
            length = Math.Abs(maxX - minX);
        }

        [BurstCompile]
        private struct BuildDataJob : IJobParallelFor
        {
            [ReadOnly] public Vector3 translation;
            [ReadOnly] public Quaternion rotation;
            [ReadOnly] public Vector3 scale;

            [ReadOnly] public NativeArray<Vector3> meshVertices;
            [ReadOnly] public NativeArray<Vector3> meshNormals;

            public NativeArray<MeshVertexStruct> meshVertexStruct;

            public void Execute(int i)
            {
                var transformed = new MeshVertexStruct(meshVertices[i], meshNormals[i]);
                //  application of rotation
                if (rotation != Quaternion.identity)
                {
                    transformed.position = rotation * transformed.position;
                    transformed.normal = rotation * transformed.normal;
                }
                if (scale != Vector3.one)
                {
                    transformed.position = Vector3.Scale(transformed.position, scale);
                    transformed.normal = Vector3.Scale(transformed.normal, scale);
                }
                if (translation != Vector3.zero)
                {
                    transformed.position += translation;
                }

                meshVertexStruct[i] = transformed;
            }
        }

        public override bool Equals(object obj) {
            if (obj == null || GetType() != obj.GetType()) {
                return false;
            }
            var other = (SourceMesh)obj;
            return Mesh == other.Mesh &&
                translation == other.translation &&
                rotation == other.rotation &&
                scale == other.scale;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public static bool operator ==(SourceMesh sm1, SourceMesh sm2) {
            return sm1.Equals(sm2);
        }
        public static bool operator !=(SourceMesh sm1, SourceMesh sm2) {
            return sm1.Equals(sm2);
        }
    }
}
