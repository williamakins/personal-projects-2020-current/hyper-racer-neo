﻿using NaughtyAttributes;
using UnityEngine;

namespace SplineMesh
{
    public class SplineMeshData : MonoBehaviour
    {
        public Mesh renderMesh;
        public Mesh collisionMesh;
        public Material material;
        public GameObject[] decorations;
        public int[] decorationsNearestVertices;

        [HideInInspector] public int[] vertexEnums;

        [ReadOnly]
        public int vertexEnumsLength;

        [Button("Recalculate Nearest Vertices", EButtonEnableMode.Editor)]
        private void RecalculateNearestVertices()
        {
            if (renderMesh != null && decorations != null && decorations.Length > 0)
            {
                decorationsNearestVertices = new int[decorations.Length];

                for (int i = 0; i < decorationsNearestVertices.Length; ++i)
                {
                    float dist = float.MaxValue;
                    for (int j = 0; j < renderMesh.vertexCount; ++j)
                    {
                        float newDist = Vector3.Distance(decorations[i].transform.position, renderMesh.vertices[j]);

                        if (newDist < dist)
                        {
                            dist = newDist;
                            decorationsNearestVertices[i] = j;
                        }
                    }
                }
            }
        }

        [Button("Get Enum Length", EButtonEnableMode.Editor)]
        private void GetEnumLength()
        {
            vertexEnumsLength = vertexEnums.Length;
        }

        [Button("Reset Mesh Enums", EButtonEnableMode.Editor)]
        private void ResetMeshEnums()
        {
            vertexEnums = new int[0];
            vertexEnumsLength = vertexEnums.Length;
        }
    }
}