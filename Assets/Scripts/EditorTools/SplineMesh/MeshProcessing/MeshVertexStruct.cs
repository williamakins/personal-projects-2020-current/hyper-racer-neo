﻿using UnityEngine;

namespace SplineMesh
{
    public struct MeshVertexStruct
    {
        public Vector3 position;
        public Vector3 normal;
        public Vector2 uv;

        public MeshVertexStruct(Vector3 position, Vector3 normal, Vector2 uv)
        {
            this.position = position;
            this.normal = normal;
            this.uv = uv;
        }

        public MeshVertexStruct(Vector3 position, Vector3 normal) : this(position, normal, Vector2.zero)
        {
        }
    }
}