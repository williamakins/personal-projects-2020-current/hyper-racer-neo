﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using HyperRacer.Editor.MeshEnums;
using LudumDare.Controller;
using NaughtyAttributes;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

namespace SplineMesh
{
    /// <summary>
    /// Deform a mesh and place it along a spline, given various parameters.
    /// 
    /// This class intend to cover the most common situations of mesh bending. It can be used as-is in your project,
    /// or can serve as a source of inspiration to write your own procedural generator.
    /// </summary>
    [ExecuteInEditMode]
    [SelectionBase]
    [DisallowMultipleComponent]
    public class SplineMeshTiling : MonoBehaviour
    {
        private enum MeshType
        {
            RenderMesh,
            CollisionMesh
        }

        [System.Serializable]
        private struct SegmentPrefabs
        {
            public GameObject prefab;
            public bool locked;

            [HideInInspector] public SplineMeshData meshData;
            [HideInInspector] public GameObject generatedPrefab;
            [HideInInspector] public GameObject decorationsParent;
            [HideInInspector] public GameObject[] decorations;
        }

        [SerializeField, ReorderableList]
        private SegmentPrefabs[] segmentPrefabs;

        [SerializeField, HideInInspector] private string meshFolder = "";

        [SerializeField] private Spline spline = null;

        [Tooltip("Translation to apply on the mesh before bending it.")]
        public Vector3 translation;
        [Tooltip("Rotation to apply on the mesh before bending it.")]
        public Vector3 rotation;
        [Tooltip("Scale to apply on the mesh before bending it.")]
        public Vector3 scale = Vector3.one;

        [SerializeField] private bool displayAlertOnFinish = false;
        [SerializeField] private bool hideMeshCompletionText = true;

        [Button("Refresh Serialized Fields", EButtonEnableMode.Editor)] private void RefreshSerializedFields() => OnRefreshSerializedFields();
        [Button("Generate Decorations", EButtonEnableMode.Editor)] private void GenerateDecorations() => OnGenerateDecorations();
        [Button("Generate Mesh Data", EButtonEnableMode.Editor)] private void GenerateMeshData() => OnBeginGenerateMeshData();
        [Button("Lock All Segments", EButtonEnableMode.Editor)] private void LockAllSegments() => OnLockAllSegments();
        [Button("Unlock All Segments", EButtonEnableMode.Editor)] private void UnlockAllSegments() => OnUnlockAllSegments();

        [Button("Update Mesh Enums", EButtonEnableMode.Editor)] private void UpdateMeshEnums() => OnUpdateMeshEnums();
        [Button("Show All Vertex Enums", EButtonEnableMode.Editor)] private void ShowAllVertexEnums() => OnShowAllVertexEnums();
        [Button("Hide All Vertex Enums", EButtonEnableMode.Editor)] private void HideAllVertexEnums() => OnHideAllVertexEnums();

        private Stopwatch generateTimer;
        private int numMeshSegments = 0;

        private const string folderPath = "Assets/Models/GeneratedMaps/";

        private void OnValidate()
        {
            if (segmentPrefabs != null && segmentPrefabs.Length != numMeshSegments)
            {
                meshFolder = folderPath + SceneManager.GetActiveScene().name;
                OnRefreshSerializedFields();
            }
        }

        private void OnRefreshSerializedFields()
        {
            if (spline == null || spline.curves.Count == 0)
            {
                Debug.LogWarning("Spline is null and/or contains no data points");
                return;
            }

            numMeshSegments = spline.curves.Count;

            SegmentPrefabs[] tempMeshSegments = segmentPrefabs;
            segmentPrefabs = new SegmentPrefabs[numMeshSegments];

            if (!Directory.Exists(meshFolder))
            {
                Directory.CreateDirectory(meshFolder);
            }

            //preserve the old serialized data by moving it into the new array
            for (int i = 0; i < numMeshSegments; ++i)
            {
                if (i < tempMeshSegments.Length)
                {
                    segmentPrefabs[i] = tempMeshSegments[i];
                }

                string generatedName = "Segment " + i;
                Transform generatedTranform = transform.Find(generatedName);
                segmentPrefabs[i].generatedPrefab = generatedTranform != null ? generatedTranform.gameObject : UOUtility.Create(generatedName, gameObject);

                if (segmentPrefabs[i].prefab != null && segmentPrefabs[i].prefab.TryGetComponent(out SplineMeshData meshData))
                {
                    segmentPrefabs[i].meshData = meshData;

                    string decorationName = "Decorations";
                    Transform decorationTransform = segmentPrefabs[i].generatedPrefab.transform.Find(decorationName);
                    segmentPrefabs[i].decorationsParent = decorationTransform != null ? decorationTransform.gameObject : UOUtility.Create(decorationName, segmentPrefabs[i].generatedPrefab);
                }
                else
                {
                    Debug.LogWarning("Mesh Segment " + i + " doesn't contain mesh prefab and/or mesh data component, please attach it");
                }

                string meshDirectory = meshFolder + "/" + generatedName;
                if (!Directory.Exists(meshDirectory))
                {
                    Directory.CreateDirectory(meshDirectory);
                }
            }
        }

        private void OnBeginGenerateMeshData()
        {
            if(EditorUtility.DisplayDialog("Generate Mesh Data", "Are you sure you want to generate the mesh data for unlocked segments, for complex meshes and splines this can take some time.", "Yes", "No"))
            {
                generateTimer = new Stopwatch();
                generateTimer.Start();

                OnRefreshSerializedFields();
                StartCoroutine(CreateMeshes());
                StartCoroutine(ReEnableComponents());

                generateTimer.Stop();
                TimeSpan ts = generateTimer.Elapsed;
                string elapsedTime = String.Format("{1:00} Minutes, {2:000} Seconds and {3:000} Milliseconds", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
                string elapsedTimeSmall = String.Format("{1:00} Mins, {2:000} Secs and {3:000} Millisecs", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);

                Debug.Log("<color=red>\u2605</color><color=green>Generation Finished! <color=orange>Total Runtime: " + elapsedTime + "</color></color><color=red>\u2605</color>");

                if (displayAlertOnFinish)
                {
                    EditorUtility.DisplayDialog("The mesh generation process has finished!", "It took " + elapsedTime + " to complete.", "Finally!");
                }
            }
        }

        private void OnLockAllSegments()
        {
            for (int i = 0; i < numMeshSegments; ++i)
            {
                segmentPrefabs[i].locked = true;
            }
        }

        private void OnUnlockAllSegments()
        {
            for (int i = 0; i < numMeshSegments; ++i)
            {
                segmentPrefabs[i].locked = false;
            }
        }

        private void OnShowAllVertexEnums()
        {
            for (int i = 0; i < numMeshSegments; ++i)
            {
                ShowOrHideMeshEnum(i, true);
            }
        }

        private void OnHideAllVertexEnums()
        {
            for (int i = 0; i < numMeshSegments; ++i)
            {
                ShowOrHideMeshEnum(i, false);
            }
        }

        private void ShowOrHideMeshEnum(int index, bool showEnums)
        {
            Transform segmentTransform = segmentPrefabs[index].generatedPrefab.transform;

            GameObject renderMesh = segmentTransform.Find("renderMesh").gameObject;
            if (renderMesh != null)
            {
                if (renderMesh.TryGetComponent(out MeshRenderer meshRenderer))
                {
                    meshRenderer.enabled = !showEnums;
                }
            }

            GameObject collisionMesh = segmentTransform.Find("collisionMesh").gameObject;

            if (collisionMesh != null)
            {
                if (collisionMesh.TryGetComponent(out MeshEnums meshEnums))
                {
                    meshEnums.OnShowOrHideMeshEnums(showEnums);
                }
            }
        }

        private void OnUpdateMeshEnums()
        {
            for (int i = 0; i < numMeshSegments; ++i)
            {
                GameObject collisionMesh = segmentPrefabs[i].generatedPrefab.transform.Find("collisionMesh").gameObject;

                if (collisionMesh.TryGetComponent(out MeshEnums meshEnums))
                {
                    meshEnums.vertexEnums = segmentPrefabs[i].meshData.vertexEnums;
                }

                ShowOrHideMeshEnum(i, true);
            }
        }

        private IEnumerator CreateMeshes()
        {
            for (int i = 0; i < numMeshSegments; ++i)
            {
                if (!segmentPrefabs[i].locked)
                {
                    CreateMesh(i);
                }
            }

            yield return null;
        }

        private void CreateMesh(int index)
        {
            Mesh renderMesh = segmentPrefabs[index].meshData?.renderMesh ?? null;
            Mesh collisionMesh = segmentPrefabs[index].meshData?.collisionMesh ?? null;

            Material mat = segmentPrefabs[index].meshData?.material ?? null;

            if (renderMesh != null && mat != null)
            {
                GameObject go = FindOrCreate("renderMesh", renderMesh, mat, index, MeshType.RenderMesh);

                MeshBender meshBender = go.GetComponent<MeshBender>();
                meshBender.SetInterval(spline.curves[index]);
                meshBender.HideMeshCompletionText(hideMeshCompletionText);
            }

            if (collisionMesh != null)
            {
                GameObject go = FindOrCreate("collisionMesh", collisionMesh, null, index, MeshType.CollisionMesh);

                MeshBender meshBender = go.GetComponent<MeshBender>();
                meshBender.SetInterval(spline.curves[index]);
                meshBender.HideMeshCompletionText(hideMeshCompletionText);
            }
        }

        private GameObject FindOrCreate(string name, Mesh mesh, Material material, int index, MeshType meshType)
        {
            Transform childTransform = segmentPrefabs[index].generatedPrefab.transform.Find(name);
            GameObject res;
            if (childTransform == null)
            {
                res = UOUtility.Create(name,
                    segmentPrefabs[index].generatedPrefab,
                    typeof(MeshFilter),
                    meshType == MeshType.RenderMesh ? typeof(MeshRenderer) : null,
                    typeof(MeshBender),
                    meshType == MeshType.CollisionMesh ? typeof(MeshCollider) : null,
                    meshType == MeshType.CollisionMesh ? typeof(MeshEnums) : null
                );
                //meshType == MeshType.CollisionMesh || meshType == MeshType.CollisionMeshOuter ? typeof(MeshVisualizer) : null);

                res.isStatic = true;
            }
            else
            {
                res = childTransform.gameObject;
            }

            if (material != null)
            {
                res.GetComponent<MeshRenderer>().material = material;
            }

            MeshBender mb = res.GetComponent<MeshBender>();
            mb.Source = SourceMesh.Build(mesh)
                .Translate(translation)
                .Rotate(Quaternion.Euler(rotation))
                .Scale(scale);
            mb.Mode = MeshBender.FillingMode.StretchToInterval;

            mb.HideMeshCompletionText(hideMeshCompletionText);
            mb.SetFolderDirectory(meshFolder + "/Segment " + index);

            if (res.TryGetComponent(out MeshEnums meshEnums))
            {
                meshEnums.vertexEnums = segmentPrefabs[index].meshData.vertexEnums;
            }

            return res;
        }

        private void OnGenerateDecorations()
        {
            if (segmentPrefabs != null && segmentPrefabs.Length > 0)
            {
                for (int i = 0; i < segmentPrefabs.Length; ++i)
                {
                    if (!segmentPrefabs[i].locked)
                    {
                        DestroyDecorations(i);
                        if (segmentPrefabs[i].meshData.decorations.Length > 0)
                        {
                            segmentPrefabs[i].decorations = UOUtility.CreateDecorations(
                                segmentPrefabs[i].meshData.decorations,
                                segmentPrefabs[i].meshData.decorationsNearestVertices,
                                segmentPrefabs[i].decorationsParent,
                                spline.nodes[i].Direction,
                                rotation,
                                segmentPrefabs[i].generatedPrefab.transform.Find("renderMesh").GetComponent<MeshBender>().result
                            );
                        }
                    }
                }
            }
        }

        private void DestroyDecorations(int index)
        {
            for (int i = 0; i < segmentPrefabs[index].decorations.Length; ++i)
            {
                DestroyImmediate(segmentPrefabs[index].decorations[i]);
            }

            segmentPrefabs[index].decorations = null;
        }

        private IEnumerator ReEnableComponents()
        {
            yield return null;

            //this is done to resolve a small visual issue where after generation you can't see the meshes
            if (spline != null)
            {
                spline.gameObject.SetActive(false);
                spline.gameObject.SetActive(true);
            }
        }
    }
}

#endif