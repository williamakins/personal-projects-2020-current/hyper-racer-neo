﻿using UnityEngine;
using System.Linq;
using System;
using System.Collections.Generic;

namespace SplineMesh {
    public static class UOUtility {
        public static GameObject Create(string name, GameObject parent, params Type[] components)
        {
            List<Type> newComponets = new List<Type>();
            for (int i = 0; i < components.Length; ++i)
            {
                if (components[i] != null)
                {
                    newComponets.Add(components[i]);
                }
            }

            var res = new GameObject(name, newComponets.ToArray());
            res.transform.parent = parent.transform;
            res.transform.localPosition = Vector3.zero;
            res.transform.localScale = Vector3.one;
            res.transform.localRotation = Quaternion.identity;

            return res;
        }

        public static GameObject[] CreateDecorations(GameObject[] decorations, int[] nearestVertices, GameObject parent, Vector3 splineDirection, Vector3 meshRotationOffset, Mesh mesh)
        {
            GameObject[] spawnedObjects = new GameObject[decorations.Length];

            for (int i = 0; i < decorations.Length; ++i)
            {
                if (mesh != null)
                {
                    spawnedObjects[i] = Instantiate(decorations[i], parent.transform);
                    spawnedObjects[i].name = decorations[i].name;

                    spawnedObjects[i].transform.localPosition = mesh.vertices[nearestVertices[i]] + decorations[i].transform.position;
                    spawnedObjects[i].transform.rotation = Quaternion.LookRotation(spawnedObjects[i].transform.localPosition + splineDirection) * Quaternion.Euler(meshRotationOffset) * decorations[i].transform.rotation;
                    spawnedObjects[i].transform.localScale = decorations[i].transform.localScale;
                }
            }

            return spawnedObjects;
        }

        public static GameObject Instantiate(GameObject prefab, Transform parent) {
            var res = UnityEngine.Object.Instantiate(prefab, parent);
            res.transform.localPosition = Vector3.zero;
            res.transform.localRotation = Quaternion.identity;
            res.transform.localScale = Vector3.one;
            return res;
        }

        public static void Destroy(GameObject go) {
            if (Application.isPlaying) {
                UnityEngine.Object.Destroy(go);
            } else {
                UnityEngine.Object.DestroyImmediate(go);
            }
        }

        public static void Destroy(Component comp) {
            if (Application.isPlaying) {
                UnityEngine.Object.Destroy(comp);
            } else {
                UnityEngine.Object.DestroyImmediate(comp);
            }
        }

        public static void DestroyChildren(GameObject go) {
            var childList = go.transform.Cast<Transform>().ToList();
            foreach (Transform childTransform in childList) {
                Destroy(childTransform.gameObject);
            }
        }
    }
}
