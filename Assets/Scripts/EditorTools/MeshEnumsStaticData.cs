﻿using UnityEngine;
using System.Collections.Generic;

namespace HyperRacer.Editor.MeshEnums
{
    public static class MeshEnumsStaticData
    {
        public enum MeshEnum
        {
            Standard = 0,
            Slow = 1,
            Fast = 2,
            Hole = 3,
            Lap = 4
        }

        public static Dictionary<MeshEnum, Color> MeshEnumColors = new Dictionary<MeshEnum, Color>();

        public static void PopulateDict()
        {
            MeshEnumColors.TryAdd(MeshEnum.Standard, Color.blue);
            MeshEnumColors.TryAdd(MeshEnum.Slow, Color.red);
            MeshEnumColors.TryAdd(MeshEnum.Fast, Color.green);
            MeshEnumColors.TryAdd(MeshEnum.Hole, new Color(0, 0, 0, 0));
            MeshEnumColors.TryAdd(MeshEnum.Lap, Color.cyan);
        }

        public static void TryAdd(this Dictionary<MeshEnum, Color> dictionary, MeshEnum key, Color value)
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, value);
            }
        }
    }
}