﻿using NaughtyAttributes;
using UnityEngine;

namespace HyperRacer.Editor.MeshEnums
{
    public class MeshEnums : MonoBehaviour
    {
        [HideInInspector] public int[] vertexEnums;

        private bool showEnums = false;
        private MeshRenderer meshRenderer = null;
        private Mesh mesh = null;
        private Color[] meshColors = null;

        [Button("Show/Hide Mesh Enums", EButtonEnableMode.Editor)]
        private void ShowOrHideMeshEnums()
        {
            showEnums = !showEnums;

            OnShowOrHideMeshEnums(showEnums);
        }

        public void OnShowOrHideMeshEnums(bool show)
        {
            showEnums = show;

            if (show)
            {
                if (gameObject.TryGetComponent(out MeshRenderer meshRenderer))
                {
                    this.meshRenderer = meshRenderer;
                }
                else
                {
                    mesh = gameObject.GetComponent<MeshFilter>().sharedMesh;
                    this.meshRenderer = gameObject.AddComponent<MeshRenderer>();
                    this.meshRenderer.material = Resources.Load<Material>("Materials/M_VertexColors");

                    meshColors = new Color[vertexEnums.Length];

                    if (vertexEnums != null && meshColors != null && vertexEnums.Length == meshColors.Length)
                    {
                        MeshEnumsStaticData.PopulateDict();

                        UpdateAllColors();
                    }
                }
            }
            else
            {
                if (meshRenderer != null)
                {
                    DestroyImmediate(meshRenderer);
                    meshRenderer = null;
                }
            }
        }

        private void UpdateAllColors()
        {
            for (int i = 0; i < meshColors.Length; ++i)
            {
                meshColors[i] = MeshEnumsStaticData.MeshEnumColors[(MeshEnumsStaticData.MeshEnum)vertexEnums[i]];
            }

            mesh.colors = meshColors;
        }
    }
}