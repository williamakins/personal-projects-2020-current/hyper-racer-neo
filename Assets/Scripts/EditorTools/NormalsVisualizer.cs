﻿#if UNITY_EDITOR

using NaughtyAttributes;
using UnityEditor;
using UnityEngine;

namespace HyperRacer.Editor
{
    [ExecuteInEditMode]
    public class NormalsVisualizer : MonoBehaviour
    {
        private bool showNormals = false;
        private Mesh mesh = null;

        [SerializeField] private MeshFilter meshFilter = null;

        [Header("Set the max number of normals to visualize (-1 = all)")]
        [SerializeField] private int maxNormals = -1;

        [Button("Show/Hide Normals", EButtonEnableMode.Editor)]
        private void ShowOrHideNormals()
        {
            showNormals = !showNormals;
            mesh = meshFilter.sharedMesh;
        }

        private void OnDrawGizmos()
        {
            if (!showNormals || mesh == null)
            {
                return;
            }

            int numOfNormals = maxNormals == -1 ? mesh.vertexCount : maxNormals;
            if (maxNormals > mesh.vertexCount)
            {
                numOfNormals = mesh.vertexCount;
            }

            for (int i = 0; i < numOfNormals; i++)
            {
                Handles.matrix = meshFilter.transform.localToWorldMatrix;
                Handles.color = Color.yellow;
                Handles.DrawLine(mesh.vertices[i], mesh.vertices[i] + mesh.normals[i]);
            }
        }
    }
}

#endif