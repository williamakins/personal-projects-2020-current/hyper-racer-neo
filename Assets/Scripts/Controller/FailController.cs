﻿using HyperRacer.Core.ServiceLocator;
using LudumDare.Controller;
using LudumDare.Core.EventManager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailController : AbstractController
{
    private IEventManager<Events> eventManager = null;
    private void Start()
    {
        eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            eventManager.TriggerEvent(Events.FailConditionMet);
        }
    }
}
