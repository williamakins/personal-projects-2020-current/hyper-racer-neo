﻿using System.Collections.Generic;
using HyperRacer.Core.ServiceLocator;
using LudumDare.Controller;
using LudumDare.Core;
using LudumDare.Core.EventManager;
using LudumDare.Model;
using UnityEngine;

public class GameController : AbstractController
{
    [SerializeField] private GameObject objectToSpawn;

    private GameModel gameModel = null;

    private IEventManager<Events> eventManager;
    protected override void Awake()
    {
        gameModel = Models.GetModel<GameModel>();
    }

    protected override void Start()
    {
        InitializeData();

        eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();
        eventManager.RegisterEvent(Events.OnLap, OnLap);
        eventManager.RegisterEvent(Events.ResetMap, OnResetMap);
    }

    private void InitializeData()
    {
        gameModel.lap = 1;

        if (gameModel.ghostCars != null && gameModel.ghostCars.Count > 0)
        {
            for (int i = 0; i < gameModel.ghostCars.Count; ++i)
            {
                Destroy(gameModel.ghostCars[i]);
            }
        }

        gameModel.ghostCars = new List<GameObject>();

        gameModel.ResetPosData();
    }

    private void OnLap(Events arg1, object[] arg2)
    {
        gameModel.lap++;

        if (objectToSpawn != null)
        {
            GameObject car = Instantiate(objectToSpawn);
            gameModel.ghostCars.Add(car);
        }
        else
        {
            Debug.LogWarning("Cannot instantiate ghost, game object is null");
        }
    }

    private void OnResetMap(Events arg1, object[] arg2)
    {
        InitializeData();
    }

    protected override void OnDestroy()
    {
        eventManager.DeregisterEvent(Events.OnLap, OnLap);
        eventManager.DeregisterEvent(Events.ResetMap, OnResetMap);
    }
}
