﻿using HyperRacer.Core.ServiceLocator;
using LudumDare.Core.EventManager;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class Countdown : MonoBehaviour
{

    [SerializeField]
    private TextMeshProUGUI tmp;
    [SerializeField]
    private string[] countdownStrings;
    [SerializeField]
    private UnityEvent unityEvents;

    private IEventManager<Events> eventManager;
    // Start is called before the first frame update
    void Start()
    {
        eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();
        eventManager.RegisterEvent(Events.StartCountdown, CountdownStarted);
    }
    private void OnDestroy()
    {
        eventManager.DeregisterEvent(Events.StartCountdown, CountdownStarted);
    }

    private void CountdownStarted(Events arg1, object[] arg2)
    {
        if(arg2[0] is float)
        {
            float value = (float)arg2[0];
            if(value >= 0 && value < countdownStrings.Length)
            {
                tmp.text = countdownStrings[(int)value];
                unityEvents?.Invoke();
            }
            
        }
    }
}
