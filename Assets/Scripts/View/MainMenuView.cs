﻿using HyperRacer.Core.ServiceLocator;
using HyperRacer.Helper;
using HyperRacer.Services.AudioManager;
using LudumDare.Core;
using LudumDare.Model;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LudumDare.View
{
    public class MainMenuView : AbstractView
    {
        [SerializeField] private GameObject mainContainer = null;
        [SerializeField] private GameObject levelSelectContainer = null;

        private GameModel gameModel = null;
        private IAudioManager audioManager = null;

        protected override void Start()
        {
            base.Start();

            gameModel = Models.GetModel<GameModel>();
            audioManager = ServiceLocator.Current.Get<IAudioManager>();

            audioManager.PlaySound("music_dasFunky");
        }

        public void OnBtnPlayPressed()
        {
            mainContainer.SetActive(false);
            levelSelectContainer.SetActive(true);
        }

        public void OnBtnQuitPressed()
        {
            Application.Quit();
        }

        public void OnBtnBackPressed()
        {
            mainContainer.SetActive(true);
            levelSelectContainer.SetActive(false);
        }

        public void OnGameLoadPressed(string scene)
        {
            SceneManagerHelper.UnloadScene("Main");

            SceneManagerHelper.LoadSceneAsync(scene, LoadSceneMode.Additive);
            SceneManagerHelper.LoadSceneAsync("HUDScene", LoadSceneMode.Additive);
            SceneManagerHelper.LoadSceneAsync("MusicScene", LoadSceneMode.Additive);
            SceneManagerHelper.LoadSceneAsync("EndScene", LoadSceneMode.Additive, false);

            gameModel.level = scene;
        }
    }
}