﻿using System;
using HyperRacer.Core.ServiceLocator;
using LudumDare.Core;
using LudumDare.Core.EventManager;
using LudumDare.Model;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace LudumDare.View
{
    public class HudView : AbstractView
    {
        [SerializeField] private Text lapText = null;
        [SerializeField] private Text speedText = null;

        //[SerializeField] private float mphPerUnit = 120.0f;

        [SerializeField]
        private UnityEvent lapCompleteEvents;

        private GameModel gameModel = null;
        private CarModel carModel = null;

        private bool deathConditionMet = false;
        //private float speedModifier = 3.0f;

        private IEventManager<Events> eventManager;
        protected override void Start()
        {
            base.Start();

            gameModel = Models.GetModel<GameModel>();
            carModel = Models.GetModel<CarModel>();
            //mphPerUnit = carModel.mphPerUnit;
            //gameModel.OnLapUpdated += LapUpdated;
            carModel.OnCurrentSpeedUpdated += CurrentSpeedUpdated;

            RefreshAll();
            eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();

            eventManager.RegisterEvent(Events.FailConditionMet, OnDeathConditionMet);
            eventManager.RegisterEvent(Events.OnLap, OnLapUpdated);
            eventManager.RegisterEvent(Events.ResetMap, OnResetMap);
        }

        private void Update()
        {
            //if (deathConditionMet == true)
            //{
            //    int displayedSpeed = (int)((carModel.currentSpeed - speedModifier) * carModel.mphPerUnit);
            //    if (displayedSpeed < 0)
            //    {
            //        displayedSpeed = 0;
            //    }

            //    speedText.text = displayedSpeed.ToString();
            //}
        }

        private void OnLapUpdated(Events eventName, object[] objects)
        {
            LapUpdated();
        }

        private void OnResetMap(Events arg1, object[] arg2)
        {
            RefreshAll();
        }

        private void OnDeathConditionMet(Events eventName, object[] objects)
        {
            deathConditionMet = true;
        }

        private void RefreshAll()
        {
            LapUpdated();
            CurrentSpeedUpdated(carModel.currentSpeed);
        }

        private void CurrentSpeedUpdated(float speed)
        {
            int displayedSpeed = (int)(speed * carModel.mphPerUnit);
            if (displayedSpeed < 0)
            {
                displayedSpeed = 0;
            }

            speedText.text = displayedSpeed.ToString();
        }

        private void LapUpdated()
        {
            lapText.text = "Lap: " + gameModel.lap;

            if (gameModel.lap > 1)
            {
                lapCompleteEvents?.Invoke();
            }
        }

        protected override void OnDestroy()
        {
            //gameModel.OnLapUpdated -= LapUpdated;
            carModel.OnCurrentSpeedUpdated -= CurrentSpeedUpdated;

            eventManager.DeregisterEvent(Events.FailConditionMet, OnDeathConditionMet);
            eventManager.DeregisterEvent(Events.OnLap, OnLapUpdated);
            eventManager.DeregisterEvent(Events.ResetMap, OnResetMap);
        }
    }
}