﻿using System.Collections;
using System.Collections.Generic;
using HyperRacer.Helper;
using LudumDare.View;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoadingView : AbstractView
{
    [SerializeField] private float delayHideLoadingScreen = 0.5f;

    [SerializeField] private Slider loadingSlider = null;
    [SerializeField] private TextMeshProUGUI txtLoadingPercent = null;

    protected override void Awake()
    {
        SceneManagerHelper.AllLoadingComplete += OnAllLoadingComplete;
        SceneManagerHelper.SceneLoadingBegun += StartLoadProgress;
    }

    protected override void OnDestroy()
    {
        SceneManagerHelper.AllLoadingComplete -= OnAllLoadingComplete;
        SceneManagerHelper.SceneLoadingBegun -= StartLoadProgress;
    }

    private void Update()
    {
        if (SceneManagerHelper.ScenesLoading.Count > 0)
        {
            float totalProgress = 0.0f;

            foreach (KeyValuePair<string, float> sceneLoadStatus in SceneManagerHelper.ScenesLoading)
            {
                totalProgress += sceneLoadStatus.Value;
            }

            float progress = totalProgress / SceneManagerHelper.ScenesLoading.Count;
            progress = Mathf.Clamp01(progress / 0.9f);

            loadingSlider.value = progress;
            txtLoadingPercent.text = progress * 100.0f + "%";
        }
    }

    private void StartLoadProgress(string sceneName, AsyncOperation op)
    {
        StartCoroutine(UpdateLoadProgress(sceneName, op));
    }

    private IEnumerator UpdateLoadProgress(string sceneName, AsyncOperation op)
    {
        while (!op.isDone)
        {
            SceneManagerHelper.ScenesLoading[sceneName] = op.progress;

            yield return null;
        }
    }

    private void OnAllLoadingComplete()
    {
        StartCoroutine(HideLoadingScreenDelay());
    }

    private IEnumerator HideLoadingScreenDelay()
    {
        yield return new WaitForSeconds(delayHideLoadingScreen);

        SceneManagerHelper.HideLoadingScene();
    }
}