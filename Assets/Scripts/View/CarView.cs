﻿using System.Runtime.CompilerServices;
using LudumDare.Core;
using LudumDare.Model;
using UnityEngine;
using LudumDare.Core.EventManager;
using LudumDare.Controller;
using HyperRacer.Core.ServiceLocator;

namespace LudumDare.View
{
    public class CarView : AbstractView
    {
        [SerializeField] private Transform rayOriginPoint = null;
        [SerializeField] private Transform carTransform = null;
        [SerializeField] private Rigidbody carRigidbody = null;
        [SerializeField] private LayerMask mask;

        [SerializeField] private AudioSource engineAudioSource = null;
        [SerializeField] private AudioSource soundsAudioSource = null;

        [SerializeField] private Material carMat = null;

        [SerializeField]
        private float startDuration = 3.0f;

        [SerializeField]
        private LeadCarPositionsController leadPositionController;

        private CarModel carModel = null;
        private AudioModel audioModel = null;

        private static readonly int Vector11AFDC38C = Shader.PropertyToID("Vector1_1AFDC38C");
        private static readonly int Vector178665D25 = Shader.PropertyToID("Vector1_78665D25");

        private GenericStateMachine stateMachine;

        private IEventManager<Events> eventManager = null;
        protected override void Start()
        {
            base.Start();

            carModel = Models.GetModel<CarModel>();
            audioModel = Models.GetModel<AudioModel>();


            eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();
            stateMachine = new GenericStateMachine();

            DrivingState drivingState = new DrivingState(carRigidbody, rayOriginPoint, transform, mask, leadPositionController);
            StartingState startingState = new StartingState(startDuration);
            DeathState deathState = new DeathState(carRigidbody, transform, carModel, carMat);

            startingState.transitions.Add(new Transition(drivingState, () => startingState.TimeIsUp()));
            drivingState.transitions.Add(new Transition(deathState, () => drivingState.PlayerFailed()));
            deathState.transitions.Add(new Transition(startingState, () => deathState.IsDead()));
            drivingState.transitions.Add(new Transition(startingState, () => drivingState.GamePaused()));

            stateMachine.Initialize(startingState);


            PlayCarEngineAudio();

            eventManager.RegisterEvent(Events.OnLap, OnLapUpdated);
            eventManager.RegisterEvent(Events.FailConditionMet, OnDeathConditionMet);
            eventManager.RegisterEvent(Events.ResetMap, OnResetMap);
        }

        private void OnDrawGizmos()
        {
            if (rayOriginPoint != null)
            {
                Gizmos.DrawWireSphere(rayOriginPoint.position, 0.03f);
            }
        }

        private void PlayCarEngineAudio()
        {
            engineAudioSource.clip = audioModel.GetSound("Shepard-tone");
            engineAudioSource.Play();
        }

        private void OnLapUpdated(Events arg1, object[] arg2)
        {
            soundsAudioSource.volume = 0.8f;
            soundsAudioSource.PlayOneShot(audioModel.GetSound("NewLap"));
        }

        private void OnDeathConditionMet(Events eventName, object[] objects)
        {
            engineAudioSource.Stop();

            soundsAudioSource.volume = 0.6f;
            soundsAudioSource.PlayOneShot(audioModel.GetSound("death"));
        }

        private void Update()
        {
            if(stateMachine.GetCurrentState() != null)
            {
                stateMachine.Update();
            }
        }

        private void FixedUpdate()
        {
            if (stateMachine.GetCurrentState() != null)
            {
                stateMachine.FixedUpdate();
            }
        }

        private void LateUpdate()
        {
            if (stateMachine.GetCurrentState() != null)
            {
                stateMachine.LateUpdate();
            }
        }

        private void OnThrottleUpdated(float throttle) {}

        private void OnResetMap(Events eventName, object[] objects)
        {
            transform.position = carModel.startingPos;
            transform.rotation = carModel.startingRot;
        }

        protected override void OnDestroy()
        {
            carMat.SetFloat(Vector11AFDC38C, 0.168f);
            carMat.SetFloat(Vector178665D25, 30.0f);

            eventManager.DeregisterEvent(Events.OnLap, OnLapUpdated);
            eventManager.DeregisterEvent(Events.FailConditionMet, OnDeathConditionMet);
            eventManager.DeregisterEvent(Events.ResetMap, OnResetMap);
        }
    }
}