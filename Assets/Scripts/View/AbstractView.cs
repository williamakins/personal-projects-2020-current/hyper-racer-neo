﻿using UnityEngine;

namespace LudumDare.View
{
    public class AbstractView : MonoBehaviour
    {
        protected virtual void Awake()
        {
        }

        protected virtual void Start()
        {
        }

        protected virtual void OnDestroy()
        {
        }
    }
}