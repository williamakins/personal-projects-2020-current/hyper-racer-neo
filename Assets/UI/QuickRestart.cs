﻿using HyperRacer.Core.ServiceLocator;
using HyperRacer.Helper;
using LudumDare.Core;
using LudumDare.Core.EventManager;
using LudumDare.Model;
using UnityEngine;

public class QuickRestart : MonoBehaviour
{
    GameModel model = null;
    MainInput input = null;


    private IEventManager<Events> eventManager;
    private void Start()
    {
        model = Models.GetModel<GameModel>();
        input = new MainInput();
        input.Car.Restart.performed += x => Restart();
        input.Car.Restart.Enable();
        eventManager = ServiceLocator.Current.Get<IEventManager<Events>>();
    }

    private void Restart()
    {
        SceneManagerHelper.ShowHideAllLoadedScenes(true);
        SceneManagerHelper.TryShowHideScene("EndScene", false);

        eventManager.TriggerEvent(Events.ResetMap);
    }
}